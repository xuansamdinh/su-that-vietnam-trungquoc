Nguồn: sackhiem.net

# Chú Dẫn Của Nhà Xuất Bản

Cuốn sách Sự thật về quan hệ Việt Nam - Trung Quốc trong 30 năm qua là một văn kiện quan trọng của Bộ Ngoại giao nước Cộng hoà xã hội chủ nghĩa Việt Nam được công bố ngày 4 tháng 10 năm 1979 nhằm vạch trần bộ mặt phản động của bọn bành trướng Bắc Kinh đối với nước ta trong suốt một thời gian dài. Cuốn sách này gồm toàn văn bản văn kiện nói trên.

Tháng 10 năm 1979

Nhà Xuất Bản Sự Thật
- Hà Nội 10/1979 gồm 5 phần tổng cộng 108 trang



◎◎◎
# Mục lục

## Phần Thứ Nhất 

Việt Nam Trong Chiến Lược Của Trung Quốc

I- Việt Nam Trong Chiến Lược Toàn Cầu Của Trung Quốc 

II- Việt Nam Trong Chính Sách Đông Nam Á Của Trung Quốc.

## Phần Thứ Hai

Trung Quốc Với Việc Kết Thúc Cuộc Chiến Tranh Đông Dương Năm 1954.

I- Sau Điện Biên Phủ, nhân dân Việt Nam Có Khả Năng Hoàn Toàn Giải Phóng Đất Nước 

II- Lập Trường Của Trung Quốc ở Giơnevơ Khác Hẳn Lập Trường Của Việt Nam, Nhưng Phù Hợp Với Lập Trường Của Pháp.

III- Hiệp Định Giơnevơ Năm 1954 Về Đông Dương Và Sự Phản Bội Của Những Người Lãnh Đạo Trung Quốc.

## Phần Thứ Ba

Trung Quốc Với Cuộc Đấu Tranh Của Nhân Dân Việt Nam Để Giải Phóng Miền Nam, Thống Nhất Nước Nhà (1954-1975)

I- Thời Kỳ 1954-1964: Những Người Cầm Quyền Trung Quốc Ngăn Cản Nhân Dân Việt Nam Đấu Tranh Để Thực Hiện Thống Nhất Nước Nhà. 

II –Thời Kỳ 1965-1969: Làm Yếu Và Kéo Dài Cuộc Kháng Chiến Của Nhân Dân Việt Nam

III –Thời Kỳ 1969-1973 : Đàm Phán Với Mỹ Trên Lưng Nhân Dân Việt Nam 

IV- Thời Kỳ 1973-1975: Cản Trở Nhân Dân Việt Nam Giải Phóng Hoàn Toàn Miền Nam

## Phần Thứ Tư

Trung Quốc Với Nước Việt Nam Hoàn Toàn Giải Phóng Và Thống Nhất  (từ tháng 5 năm 1975 đến nay)

I -Trung Quốc Sau Thất Bại Của Mỹ ở Việt Nam

II -Điên Cuồng Chống Việt Nam Nhưng Còn Cố Giấu Mặt 

III -Điên Cuồng Chống Việt Nam Một Cách Công Khai

## Phần Thứ Năm

Chính Sách Bành Trướng Của Bắc Kinh Mối Đe Doạ Đối Với Độc Lập Dân Tộc, Hoà Bình Và Ổn Định Ở Đông Nam Châu Á.

I Hòa bình ổn định ở Đông nam châu Á và trên thế giới

II Tóm lại trong 30 năm qua…

III Cuộc đấu tranh chính nghĩa của nhân dân Việt Nam

## (hết)

# PHẦN THỨ NHẤT

VIỆT NAM TRONG CHIẾN LƯỢC CỦA TRUNG QUỐC
===

Những hành động thù địch công khai của những người lãnh đạo Trung Quốc đối với Việt Nam, mà đỉnh cao là cuộc chiến tranh xâm lược của họ ngày 17 tháng 2 năm 1979, đã làm cho dư luận thế giới ngạc nhiên trước sự thay đổi đột ngột về chính sách của Trung Quốc đối với Việt Nam.

Sự thay đổi đó không phải là điều bất ngờ, mà là sự phát triển lô-gích  của chiến lược bành trướng đại dân tộc và bá quyền nước lớn của những người lãnh đạo Trung Quốc trong 30 năm qua.

Trên thế giới chưa có những người lãnh đạo một nước nào về mặt chiến lược đã lật ngược chính sách liên minh, đổi bạn thành thù, đổi thù thành bạn nhanh chóng và toàn diện như những người lãnh đạo Trung Quốc.

Từ chỗ coi Liên Xô là đồng minh lớn nhất, họ đi đến chỗ coi Liên Xô là kẻ thù nguy hiểm nhất.

Từ chỗ coi đế quốc Mỹ là kẻ thù nguy hiểm nhất mà bản chất không bao giờ thay đổi, họ đi đến chỗ coi đế quốc Mỹ là đồng minh tin cậy, câu kết với đế quốc Mỹ và trắng trợn tuyên bố Trung Quốc là NATO (1) ở phương đông.

Từ chỗ coi phong trào giải phóng dân tộc ở châu Á, châu Phi và châu Mỹ la tinh là ``bão táp cách mạng'' trực tiếp đánh vào chủ nghĩa đế quốc và cho rằng rốt cuộc sự nghiệp cách mạng của toàn bộ giai cấp vô sản quốc tế sẽ tuỳ thuộc vào cuộc đấu tranh cách mạng của nhân dân ở khu vực này (2), họ đi đến chỗ cùng với đế quốc chống lại và phá hoại phong trào giải phóng dân tộc, ủng hộ những lực lượng phản động, như tên độc tài Pinôchê ở Chilê, các tổ chức FNLA và UNITA do Cục tình báo trung ương Mỹ (CIA) giật dây ở Angôla, vua Palêvi ở Iran, nuôi dưỡng bè lũ diệt chủng PônPốt-Iêngxary v..v…Họ ngang nhiên xuyên tạc nguyên nhân và tính chất của các cuộc đấu tranh giải phóng dân tộc trên thế giới hiện nay, coi các cuộc đấu tranh này là sản phẩm của sự tranh giành bá quyền giữa các nước lớn, chứ không phải là sự nghiệp cách mạng của nhân dân các nước.

Cùng với sự lật ngược chính sách liên minh của họ trên thế giới là những cuộc thanh trừng tàn bạo và đẫm máu ở trong nước, đàn áp những người chống đối, làm đảo lộn nhiều lần vai trò của những người trong giới cầm quyền. Có người hôm nay được coi là nhà lãnh đạo cách mạng chân chính, ngày mai trở thành kẻ thù, kẻ phản bội của cách mạng Trung Quốc; có người trong vòng mấy năm lần lượt bị đổ và được phục hồi đến hai ba lần.

Chiến lược của những người lãnh đạo Trung Quốc có những thay đổi rất lớn. Nhưng có một điều không thay đổi: đó là mục tiêu chiến lược muốn nhanh chóng đưa Trung Quốc trở thành một cường quốc bậc nhất thế giới và thực hiện mưu đồ bành trướng đại dân tộc và bá quyền nước lớn của họ đối với các nước khác.

Tại Hội nghị Ban chấp hành trung ương Đảng Cộng sản Trung Quốc năm 1956, Chủ tich Mao Trạch Đông đã nói:

``Chúng ta phải trở thành quốc gia hàng đầu về phát triển văn hoá, khoa học kỹ thuật và công nghiệp…Không thể chấp nhận rằng sau một vài chục năm, chúng ta vẫn chưa trở thành cường quốc số một trên thế giới''

Sau đó, tháng 9  năm 1959, tại Hội nghị của Quân uỷ trung ương, chủ tịch Mao Trạch Đông lại nói:

``Chúng ta phải chinh phục trái đất. Đó là mục tiêu của chúng ta''

Ngay từ khi thành lập nước Cộng hoà nhân dân Trung Hoa tháng 10 năm 1949, những người cầm quyền ở Bắc Kinh đã bắt tay đẩy mạnh việc thực hiện mục tiêu chiến lược của họ. Mặc dầu nền kinh tế của Trung Quốc còn lạc hậu, từ cuối những năm 1950 họ đã ra sức xây dựng lực lượng hạt nhân chiến lược và hiện nay đang đẩy mạnh việc thực hiện ``hiện đại hoá'' về quân sự, về sản xuất và tích trữ vũ khí hạt nhân. Trong lĩnh vực kinh tế, điều giống nhau giữa ``đại nhảy vọt'' năm 1958 và ``bốn hiện đại hoá'' mới nêu ra vài ba năm nay là cả hai kế hoạch đó đều nhằm mục tiêu chiến lược bành trướng và bá quyền của những người lãnh đạo Trung Quốc.

(1) Tức khối xâm lược Bắc Đại Tây Dương (B.T.)

(2) Cần nhắc lại rằng tại hội nghị đại biểu các Đảng Cộng Sản và Công Nhân tháng 11 năm 1960. Đoàn đại biểu Đảng Cộng Sản Trung Quốc đã đồng ý với nhận định sự phát triển của xã hội loài người.

## I- VIỆT NAM TRONG CHIẾN LƯỢC TOÀN CẦU CỦA TRUNG QUỐC

Trong chiến lược toàn cầu của những người lãnh đạo Trung Quốc, nếu họ coi Liên Xô và Mỹ là những đối tượng chủ yếu cần phải chiến thắng, thì họ coi Việt Nam là một đối tượng quan trọng cần khuất phục và thôn tính để dễ bề đạt được lợi ích chiến lược của họ.

Bước vào những năm 1950, khi bắt đầu công cuộc xây dựng lại hòng đưa nước Trung Hoa nhanh chóng trở thành cường quốc trên thế giới, những người lãnh đạo Trung Quốc phải đối phó với cuộc chiến tranh của Mỹ xâm lược Triều Tiên ở phía bắc và mối nguy cơ đe doạ an ninh của Trung Quốc ở  phía nam do cuộc chiến tranh xâm lược của thực dân Pháp ở Việt Nam gây ra. Đồng thời đế quốc Mỹ, tên đế quốc đầu sỏ và tên sen đầm quốc tế, thi hành một chính sách thù địch đối với Trung Quốc và đang ra sức tiến hành kế hoạch bao vây, cô lập Trung Quốc.

Cuộc kháng chiến thắng lợi của nhân dân Việt Nam, nhân dân Lào và nhân dân Campuchia chống thực dân Pháp đã đưa đến  Hội nghị Giơnevơ năm 1954 về Đông Dương. Pháp lo sợ thắng lợi hoàn toàn của nhân dân Việt Nam sẽ dẫn đến sự sụp đổ của hệ thống thuộc địa của chủ nghĩa đế quốc Pháp. Trung Quốc là nước cung cấp nhiều vũ khí nhất cho Việt Nam vào cuối cuộc kháng chiến của nhân dân Việt Nam chống thực dân Pháp. Những người lãnh đạo Trung Quốc đã lợi dụng tình hình đó để đứng ra làm người thương lượng chủ yếu đối với đế quốc Pháp, câu kết với chúng và cùng nhau thoả hiệp về một giải pháp có lợi cho Trung Quốc và Pháp, không có lợi cho nhân dân Việt Nam, nhân dân Lào và nhân dân Campuchia. Họ đã hy sinh lợi ích của nhân dân ba nước ở Đông Dương để bảo đảm an ninh cho Trung Quốc ở phía nam, để thực hiện mưu đồ nắm Việt Nam và Đông Dương, đồng thời để có vai trò là một nước lớn trong việc giải quyết các vấn đề quốc tế, trước hết là ở châu Á. Trong Hội nghị Giơnevơ năm 1954 mà phần đầu dành cho vấn đề Triều Tiên, nước Cộng hoà nhân dân Trung Hoa lần đầu tiên xuất hiện ngang hàng với bốn cường quốc uỷ viên thường trực của Hội đồng bảo an Liên hợp quốc. Đó là một cơ hội tốt cho những người lãnh đạo Trung Quốc mở rộng ảnh hưởng của mình ở khu vực châu Á và châu Phi. Đồng thời, họ tìm cách bắt tay với đế quốc Mỹ qua các cuộc thương lượng trực tiếp bắt đầu  Giơnevơ, về sau chuyển sang Vacsava.

Từ sau khi Hiệp định Giơnevơ năm 1954 được ký kết, đế quốc Mỹ ngày càng can thiệp sâu vào Việt Nam, nhằm biến miền Nam Việt Nam thành một thuộc địa kiểu mới và một căn cứ quân sự của Mỹ ở Đông Nam châu Á. Những người cầm quyền ở Bắc Kinh muốn duy trì lâu dài tình trạng Việt Nam bị chia cắt. Nhưng nhân dân Việt Nam đã anh dũng đứng lên chống Mỹ cứu nước và ngày càng giành được nhiều thắng lợi.

Cuối những năm 1960, đế quốc Mỹ bị sa lầy trong chiến tranh xâm lược chống Việt Nam, một cuộc chiến tranh đã làm trầm trọng thêm cuộc khủng hoảng về kinh tế, chính trị và xã hội của nước Mỹ, đồng thời làm suy yếu thêm vị trí của đế quốc Mỹ trên thế giới. Tình hình đó đã tạo điều kiện thuận lợi cho các nước xã hội chủ nghĩa và phong trào cách mạng trên thế giới phát triển mạnh mẽ và tạo cho các nước Tây Âu, Nhật Bản có cơ hội ngoi lên thành những lực lượng cạnh tranh mạnh mẽ với Mỹ. Trước tình thế ngày càng tuyệt vọng, tổng thống Nichxơn đã đề ra kế hoạch ``Việt Nam hoá'' chiến tranh và theo kinh nghiệm của đế quốc Pháp năm 1954, dùng Trung Quốc hòng giải quyết vấn đề Việt Nam với những điều kiện có lợi cho đế quốc Mỹ: rút quân Mỹ ra khỏi Việt Nam mà vẫn giữ được chế độ bù nhìn Nguyễn Văn Thiệu ở miền nam Việt Nam. Đồng thời, chính quyền Nichxơn chơi con bài Trung Quốc để gây sức ép đối với Liên Xô, chống phong trào cách mạng thế giới.

Lợi dụng sự suy yếu của đế quốc Mỹ và chiều hướng chính sách của chính quyền Nichxơn, những người lãnh đạo Trung Quốc tăng cường chống Liên Xô và thoả hiệp với Mỹ, giúp Mỹ giải quyết vấn đề Việt Nam để cố tạo nên thế ba nước lớn trên thế giới theo công thức của Kitxinhgơ về ``thế giới nhiều cực'', trong đó một trong ba cực lớn là Trung Quốc, xoá bỏ ``thế hai cực'' Mỹ và Liên Xô đã hình thành sau chiến tranh thế giới thứ hai, đồng thời dùng vấn đề Việt Nam để đổi lấy việc Mỹ rút khỏi Đài Loan. Do đó, họ lật ngược chính sách liên minh, bắt đầu từ việc coi Liên Xô là kẻ thù chủ yếu, gây ra xung đột biên giới với Liên Xô tháng 3 năm 1969, đến việc phản bội Việt Nam lần thứ hai, buôn bán với Mỹ để ngăn cản thắng lợi hoàn toàn của nhân dân Việt Nam. Trong năm 1971, họ lần lượt tiến hành chính sách ``ngoại giao bóng bàn'', đón tiếp Kitxinhgơ ở Bắc Kinh. Tiếp đó là việc khôi phục địa vị của Trung Quốc ở Liên hợp quốc và nước Cộng hoà nhân dân Trung Hoa trở thành một trong năm uỷ viên thường trực  của Hội đồng bảo an. Đỉnh cao là việc Trung Quốc tiếp tổng thống Mỹ Nichxơn và hai bên ra Thông cáo Thượng Hải tháng 2 năm 1972. Đối với những người cầm quyền Bắc Kinh, sự câu kết với đế quốc Mỹ là một bước có ý nghĩa quyết định đối với việc triển khai chiến lược toàn cầu của họ.

Cũng như sau Hiệp định Giơnevơ 1954 về Đông Dương, những người lãnh đạo Trung Quốc sau Hiệp định Pari tháng 1 năm 1973 về Việt Nam, muốn duy trì nguyên trạng ở miền nam Việt Nam. ``Cách mạng Trung Quốc và Đảng cộng sản Trung Quốc'' của chủ tịch Mao Trạch Đông viết năm 1939 do hiệu sách Tân Hoa thư điếm (Dực Nam) tái bản tháng 6 năm 1949. Trong đó có đoạn viết:

``Sau khi dùng chiến tranh đánh bại Trung Quốc, các nước đế quốc đã cướp đi nhiều nước phụ thuộc và một bộ phận của lãnh thổ Trung Quốc: Nhật chiếm Triều Tiên, Đài Loan, Lưu Cầu, quần đảo Bành Hồ và Lữ Thuận. Anh chiếm Miến Điện, Butan, Nêpan và Hương Cảng. Pháp chiếm An Nam…''

[anh1]
[anh2]

Đường vẽ chấm là ``biên giới'' của Trung Quốc theo quan điểm bành trướng, những vùng đánh số là những lãnh thổ mà nhà cầm quyền Bắc Kinh cho là đã bị nước ngoài ``chiếm mất'' bao gồm: một phần lớn đất vùng Viễn Đông và Trung Á của Liên Xô (số 1, 17, 18), Át Xam (số 6), Xích Kim (số 4), Butan (số 5), Miến Điện (số 7), Nêpan (số 3), Thái Lan (số 10), Việt Nam (số 11), Lào, Campuchia…

Vì vậy thắng lợi của nhân dân Việt Nam năm 1975 đánh sụp hoàn toàn chính quyền tay sai của Mỹ, giải phóng miền Nam, thống nhất nước nhà không chỉ là một thất bại lớn của đế quốc Mỹ mà còn là một thất bại lớn của những người cầm quyền Bắc Kinh trong việc thực hiện chiến lược toàn cầu và mưu đố bành trướng, bá quyền của họ. Từ đó, họ công khai thi hành chính sách thù địch chống Việt Nam, kể cả bằng biện pháp quân sự.

Như vậy, trong 30 năm qua, Việt Nam là một trong những nhân tố quan trọng nhất trong chiến lược toàn cầu của Trung Quốc. Vì Việt Nam có vị trí rất quan trọng trong chiến lược của Pháp giữa những năm 1950 cũng như trong chiến lược của Mỹ đầu những năm 1970, những người lãnh đạo Trung Quốc đã dùng ``con bài'' Việt Nam để câu kết với bọn đế quốc, phục vụ cho ý đồ chiến lược bành trướng đại dân tộc và bá quyền nước lớn của họ. Đồng thời họ mưu toan một mình nắm cuộc kháng chiến chống Mỹ cứu nước của nhân dân Việt Nam, một vấn đề trung tâm của đời sống chính trị quốc tế lúc bấy giờ để giương cao chiêu bài ``chống chủ nghĩa đế quốc'' hòng nắm quyền ``lãnh đạo cách mạng thế giới'', dìm ảnh hưởng của Liên Xô.

## II- VIỆT NAM TRONG CHÍNH SÁCH ĐÔNG NAM Á CỦA TRUNG QUỐC.

Đông Nam châu Á là hướng bành trướng cổ truyền trong lịch sử Trung Quốc, là khu vực mà từ lâu những người lãnh đạo nước Cộng hoà nhân dân Trung Hoa ước mơ thôn tính.

Trong năm 1936, chủ tịch Mao Trạch Đông kể chuyện với nhà báo Mỹ Etga Xnâu ở Diên An về thời trẻ của mình, đã bộc lộ ý nghĩ sau khi đọc một cuốn sách nhỏ nói đến việc Nhật chiếm Triều Tiên và Đài Loan, việc mất ``chủ quyền''  Trung Hoa ở Đông Dương, Miến Điện và nhiều nơi khác:

``Đọc xong, tôi lấy làm thất vọng đối với tương lai đất nước tôi và tôi bắt đầu nhận thức rằng bổn phận của mọi người là phải đóng góp cứu nước''. (Ét-ga Xnâu: Ngôi sao đỏ trên đất Trung Hoa, Nhà xuất bản Pen-guyn, Lân đơn, 1972, tr. 159)

Tài liệu của Đảng cộng sản Trung Quốc do chủ tịch Mao Trạch Đông viết mang tên Cách mạng Trung Quốc và Đảng cộng sản Trung Quốc, xuất bản năm 1939 có viết:

``Các nước đế quốc sau khi đánh bại Trung Quốc, đã chiếm các nước phụ thuộc của Trung Quốc: Nhật chiếm Triều Tiên, Đài Loan, Lưu Cầu, quần đảo Bành Hồ và Lữ Thuận. Anh chiếm Miến Điện, Butan, Hương Cảng, Pháp chiếm An Nam…''

Cuốn sách Sơ lược lịch sử Trung Quốc hiện đại xuất bản năm 1954 ở Bắc Kinh có bản đồ vẻ lãnh thổ Trung Quốc bao gồm cả nhiều nước chung quanh, kể cả ở Đông Nam châu Á và vùng biển Đông.

Ý đồ bành trướng của những người lãnh đạo Trung Quốc đặc biệt lộ rõ ở câu nói của chủ tịch Mao Trạch Đông trong cuộc hội đàm với đại biểu Đảng Lao động Việt Nam ở Vũ Hán năm 1963:

``Tôi sẽ làm chủ tịch của 500 triệu bần nông đưa quân xuống Đông Nam châu Á''

Cũng trong dịp này, chủ tịch Mao Trạch Đông so sánh nước Thái Lan với tỉnh Tứ Xuyên của Trung Quốc, về diện tích thì tương đương nhưng về số dân thì tỉnh Tứ Xuyên đông gấp đôi, và nói rằng Trung Quốc cần đưa người xuống Thái Lan để ở; đối với nước Lào đất rộng người thưa, chủ tịch Mao Trạch Đông cũng cho rằng Trung Quốc cần đưa người xuống Lào để ở.

[anh3]

Chủ tịch Mao Trạch Đông còn khẳng định trong cuộc họp của Bộ Chính trị ban chấp hành trung ương Đảng cộng sản Trung Quốc, tháng 8 năm 1965:

``Chúng ta phải giành cho được Đông Nam châu Á, bao gồm cả miền nam Việt Nam, Thái Lan, Miến Điện, Malayxia và Singapo…Một vùng như Đông Nam châu Á rất giàu, ở đấy có nhiều khoáng sản…xứng đáng với sự tốn kém cần thiết để chiếm lấy…Sau khi giành được Đông Nam châu Á, chúng ta có thể tăng cường được sức mạnh của chúng ta ở vùng này, lúc đó chúng ta sẽ có sức mạnh đương đầu với khối Liên Xô-Đông Âu, gió Đông sẽ thổi bạt gió Tây…''.

So với các khu vực khác trên thế giới, Đông Nam châu Á là khu vực mà Trung  Quốc có nhiều điều kiện thuận lợi nhất, có nhiều phương tiện và khả năng nhất (hơn 20 triệu Hoa kiều, các chính đảng lệ thuộc vào Đảng cộng sản Trung Quốc, Đông Nam châu Á có đường đất liền với Trung Quốc…) để thực hiện chính sách bành trướng và bá quyền nước lớn của mình. Cho nên trong 30 năm qua, những người lãnh đạo nước Cộng hoà nhân dân Trung Hoa đã dùng nhiều thủ đoạn để thực hiện chính sách bành trướng ở khu vực này, tạo điều kiện thuận lợi cho việc thực hiện chiến lược toàn cầu phản cách mạng của họ.

Họ xây dựng lực lượng hạt nhân chiến lược, phát triển lực lượng kinh tế, ỷ thế nước lớn, đe doạ bằng quân sự và hứa hẹn viện trợ về kinh tế để mua chuộc, lôi kéo hoặc gây sức ép với các nước ở khu vực này, hòng làm cho các nước đó phải đi vào quỹ đạo của họ. Họ xâm phạm lãnh thổ các nước và gây ra xung đột biên giới, dùng lực lượng tay sai hoặc trực tiếp đem quân xâm lược, hòng làm suy yếu để dễ bề khuất phục, thôn tính nước này, nước khác trong khu vực. Họ không từ bất kỳ một hành động tàn bạo nào, như họ đã dùng tập đoàn Pônpốt- Iêngxary thực hiện chính sách diệt chủng ở Campuchia. Họ dùng nhiều công cụ ở các nước Đông Nam châu Á: lực lượng Hoa kiều làm ``đạo quân thứ năm'', các tổ chức gọi là ``cộng sản'' theo mệnh lệnh của Bắc Kinh, các dân tộc thiểu số ở cấc nước thuộc khu vực này có ít nhiều nguồn gốc dân tộc ở Trung Quốc để phục vụ cho chính sách bành trướng và bá quyền của họ.

Về việc sử dụng lực lượng Hoa kiều, ý đồ của Bắc Kinh đã được thể hiện rõ nhất trong ý kiến của bộ trưởng Bộ Ngoại giao Trần Nghị:

``Singapore có trên 90% là người Trung Quốc, trong số dân hơn 1 triệu người thì hơn 90 vạn là người Trung Quốc. Cho nên Singapore hoàn toàn trở thành một quốc gia do người Trung Quốc ở đó tổ chức''. (3)

Những người lãnh đạo Trung Quốc lợi dụng mâu thuẫn giữa các dân tộc ở Đông Nam châu Á, chia rẽ các nước thuộc tổ chức ASEAN (4) với ba nước trên bán đảo Đông Dương, chia rẽ các nước với nhau như đã chia rẽ Malayxia với Inđônêxia, Miến Điện với Thái Lan…Đặc biệt họ lợi dụng tình hình ở Đông Nam châu Á là một trong những khu vực sục sôi cách mạng trên thế giới, phong trào độc lập dân tộc ngày càng phát triển và chủ nghĩa thực dân đế quốc ngày càng suy yếu để thực hiện ý đồ bành trướng của họ. Khi thực dân Pháp thất bại ở Việt Nam năm 1954, họ muốn duy trì ở miền Nam Việt Nam, Lào và Campuchia sự có mặt của Pháp là một tên đế quốc đã suy yếu để ngăn cản Mỹ, tên đế quốc đầu sỏ thâm nhập vào Đông Dương và hạn chế thắng lợi hoàn toàn của cách mạng ba nước ở Đông Dương. Khi Mỹ suy yếu và thất bại ở vùng này, họ muốn duy trì sự có mặt của Mỹ để cùng với Mỹ thống trị các nước trong khu vực. Làm như vậy, họ hy vọng dựa vào chủ nghĩa đế quốc ngăn chặn sự phát triển của cách mạng để từng bước lấp cái gọi là lỗ hổng ở Đông Nam châu Á, tiến đến gạt dần các đồng minh đế quốc để độc chiếm khu vực này. Họ tung ra luận điệu lừa bịp là ngăn chặn ảnh hưởng của Liên Xô để che dấu âm mưu đen tối của họ.

(3) Trong cuộc nói chuyện với chuyện với các đại biểu người Hoa ỏ Việt Nam đến chào mừng đoàn đại biểu Đảng và Chính phủ Trung Quốc do Thủ tướng Chu Ân Lai dẫn đầu thăm Việt Nam, tháng 5 năm 1960.

(4) Tức hội các người Đông Nam Á, thành lập năm 1967. (B.T.)

Việt Nam có một vị trí chiến lược ở Đông Nam châu Á. Trong lịch sử, bọn bành trướng phong kiến Trung Quốc đã nhiều lần xâm lược hòng thôn tính  Việt Nam, dùng Việt Nam làm bàn đạp để xâm lược các nước khác ở Đông Nam châu Á. Khi chiến tranh thế giới thứ hai kết thúc, trong các đảng cộng sản ở khu vực này chỉ có Đảng cộng sản Việt Nam giành được chính quyền và thành lập Nhà nước công nông đầu tiên trong khu vực. Cách mạng Việt Nam có ảnh hưởng to lớn do đánh thắng thực dân Pháp và đế quốc Mỹ xâm lược. Những người lãnh đạo Trung Quốc âm mưu nắm Việt Nam để nắm toàn bộ bán đảo Đông Dương, mở đường đi xuống Đông Nam châu Á. Trong cuộc gặp giữa đại biểu bốn đảng cộng sản Việt Nam, Trung Quốc, Inđônêxia và Lào tại Quảng Đông tháng 9 năm 1963, thủ tướng Chu Ân Lai nói:

``Nước chúng tôi thì lớn nhưng không có đường ra, cho nên rất mong Đảng Lao động Việt Nam mở cho một con đường mới xuống Đông Nam châu Á''.

Để làm suy yếu và nắm lấy Việt Nam, họ ra sức phá sự đoàn kết giữa ba nước ở bán đảo Đông Dương, chia rẽ ba nước với nhau, đặc biệt là chia rẽ Lào và Campuchia với Việt Nam. Đồng thời họ cố lôi kéo các nước khác ở Đông Nam châu Á đối lập với Việt Nam, vu khống, bôi xấu, hòng cô lập Việt Nam với các nước trên thế giới.

Những người cầm quyền Bắc Kinh rêu rao cái gọi là ``chủ quyền'' của họ đối với các quần đảo Hoàng Sa và Trường Sa. Đầu năm 1974, với sự đồng tình của Mỹ, họ đánh chiếm quần đảo Hoàng Sa, bộ phận lãnh thổ của Việt Nam để từng bước kiểm soát biển Đông, khống chế Việt Nam và toàn bộ Đông Nam châu Á, đồng thời khai thác tài nguyên phong phú ở vùng biển Đông.

Những người lãnh đạo Trung Quốc hy vọng tập hợp lực lượng ở Đông Nam Châu Á để tiến đến tập hợp lực lượng trên thế giới hòng thực hiện chiến lược toàn cầu của họ. Những năm 1960 họ ra sức tập hợp lực lượng ở các khu vực Á, Phi, Mỹ latinh để nắm quyền ``lãnh đạo cách mạng thế giới'' và chống Liên Xô. Để đạt mục tiêu đó, từ năm 1963 họ ráo riết vận động họp 11 đảng cộng sản, trong đó có 8 đảng ở Đông Nam châu Á hòng lập ra một thứ ``Quốc tế cộng sản'' mới do họ khống chế, xây dựng cái gọi là ``trục Bắc Kinh- Giacacta-Phnông Pênh-Bình Nhưỡng-Hà Nội'', thông qua Inđônêxia vận động triệu tập ``Hội nghị các lực lượng mới trỗi dậy'' (CONEFO) để thành lập một tổ chức quốc tế đối lập với Liên hợp quốc; đồng thời vận động tổ chức Hội nghị Á-Phi lần thứ hai (dự định họp ở Angiê năm 1965). Nhưng những người lãnh đạo Trung Quốc đã không thành công trong các kế hoạch đen tối này. Đó là vì họ đi ngược lại lợi ích của cách mạng thế giới là tăng cường hệ thống xã hội chủ nghĩa và phong trào độc lập dân tộc, dân chủ, vì họ vấp phải đường lối độc lập tự chủ trước sau như một của Việt Nam.



◎◎◎

Trong 30 năm qua, những người lãnh đạo Trung Quốc coi Việt Nam là một trong những nhân tố quan trọng hàng đầu đối với chiến lược của họ, luôn luôn tìm cách nắm Việt Nam. Muốn như vậy, nước Việt Nam phải là một nước không mạnh, bị chia cắt, không độc lập và lệ thuộc Trung Quốc. Trái lại, một nước Việt Nam độc lập, thống nhất và giàu mạnh, có đường lối độc lập tự chủ và đường lối quốc tế đúng đắn là một cản trở lớn cho chiến lược toàn cầu của những người lãnh đạo Trung Quốc, trước hết là cho chính sách bành trướng của họ ở Đông Nam châu Á. Đó là nguyên nhân vì sao trước đây họ vừa giúp, vừa kiềm chế cách mạng Việt Nam, mỗi khi Việt Nam đánh thắng đế quốc thì họ lại buôn bán, thoả hiệp với đế quốc trên lưng nhân dân Việt Nam; vì sao từ chỗ giấu mặt chống Việt Nam họ đã chuyển sang công khai thù địch với Việt Nam và đi tới trắng trợn tiến hành chiến tranh xâm lược Việt Nam.

Chính sách của những người lãnh đạo Trung Quốc muốn thôn tính Việt Nam nằm trong chính sách chung của họ đối với các nước Đông Nam châu Á cũng như đối với các nước láng giềng khác. Họ đã muốn chiếm đất đai của Ấn Độ và thực tế đã chiếm một bộ phận đất đai của Ấn Độ trong cuộc chiến tranh năm 1962; họ không muốn có một nước Ấn Độ mạnh mà họ cho rằng có thể tranh giành với họ ``vai trò lãnh đạo'' các nước Á-Phi. Họ vẫn mưu toan chiếm Mông Cổ, mặc dù họ đã công nhận nước Cộng hoà nhân dân Mông Cổ là một quốc gia độc lập. Họ muốn chiếm một phần đất đai của Liên Xô, rất không muốn có bên cạnh Trung Quốc một nước Liên Xô hùng mạnh, nên họ tìm mọi cách hạ uy thế của Liên Xô, đẩy các nước đế quốc gây chiến tranh với Liên Xô, đẩy các nước Á-Phi Mỹ la tinh chống Liên Xô. Họ tập trung mọi cố gắng để dấy lên một cuộc ``thập tự chinh quốc tế'' của các lực lượng đế quốc và phản động chống Liên Xô dưới chiêu bài ``chống bá quyền'' theo công thức của chủ tịch Mao Trạch Đông ``Ngồi trên núi xem hổ đánh nhau''. Như nhiều nhà chính trị và báo chí Tây Âu nhận định, Trung Quốc quyết tâm ``đánh Liên Xô đến người Tây Âu cuối cùng'' cũng như Trung Quốc trước đây đã quyết tâm ``đánh Mỹ đến người Việt Nam cuối cùng''.

Chiến lược quốc tế ngày nay của những người lãnh đạo Trung Quốc, mặc dù núp dưới chiêu bài nào, đã phơi trần tính chất cực kỳ phản cách mạng của nó và những người lãnh đạo Trung Quốc đã hiện nguyên hình là những người theo chủ nghĩa sô vanh nước lớn, những người dân tộc chủ nghĩa tư sản.

Chính sách ngày nay của những người lãnh đạo Trung Quốc đối với Việt Nam, mặc dù được nguỵ trang khéo léo như thế nào, vẫn chỉ là chính sách của những hoàng đế ``thiên triều'' trong mấy nghìn năm qua, nhằm thôn tính Việt Nam, biến Việt Nam thành một chư hầu của Trung Quốc.

# PHẦN THỨ HAI

TRUNG QUỐC VỚI VIỆC KẾT THÚC CUỘC CHẾN TRANH ĐÔNG DƯƠNG NĂM 1954.
===

# I- SAU ĐIỆN BIÊN PHỦ, NHÂN DÂN VIỆT NAM CÓ KHẢ NĂNG HOÀN TOÀN GIẢI PHÓNG ĐẤT NƯỚC

Năm 1945, nhân dân Việt Nam với tinh thần quật khởi, tự lực tự cường đã làm cuộc Cách mạng tháng Tám thành công, thành lập nước Việt Nam dân chủ cộng hoà. Mấy tháng sau, thực dân Pháp đã chiếm lại các tỉnh ở Nam Bộ, và từ tháng 12 năm 1946 nhân dân Việt Nam đã phải tiến hành cuộc kháng chiến lâu dài chống đế quốc Pháp xâm lược trên phạm vi cả nước  để bảo vệ độc lập, thống nhất và toàn vẹn lãnh thổ của mình.

Cuộc kháng chiến của nhân dân Việt Nam vô cùng gian khổ và hết sức anh dũng ngày càng giành được nhiều thắng lợi to lớn. các chiến thắng của nhân dân Việt Nam củng như các chiến thắng của nhân dân Lào (dưới sự lãnh đạo của Chính phủ kháng chiến Lào) và của nhân dân Campuchia (dưới sự lãnh đạo của Chính phủ kháng chiến Campuchia), đặc biệt là chiến thắng lịch sử Điện Biên Phủ của quân và dân Việt Nam đã làm thay đổi so sánh lực lượng trên chiến trường rất có lợi cho các lực lượng kháng chiến Việt Nam, Lào và Campuchia, và đặt đế quốc Pháp trước một tình thế vô cùng khó khăn.

Bộ trưởng quốc phòng Pháp R. Plêven cùng với chủ tịc Hội đồng tham mưu trưởng tướng P. Êli, tham mưu trưởng lục quân tướng Blăng, tham mưu trưởng không quân tướng Phay, sau khi đi khảo sát chiến trường Đông Dương tháng 2 năm 1954 đã đi tới một nhận xét bi quan về tình hình chiến trường như sau:

``Một sự tăng cường dù lớn đến đâu cho quân đội viễn chinh cũng không thể làm thay đổi được tình hình. Vả lại, sự cố gắng về quân sự của chính quốc đã đến giới hạn cuối cùng. Tất cả những điều mà chúng ta có thể hy vọng là tạo điều kiện quân sự thuận lợi nhất cho một giải pháp chính trị cho cuộc xung đột''. (1)

Ngày 18 thánh 5 năm 1954, lo ngại Quân đội nhân dân Việt Nam có thể tiến công đồng bằng Bắc Bộ, thủ tướng Pháp Lanien đã cử tướng Êli sang Đông Dương để truyền đạt chỉ thị cho tướng Nava, tổng chỉ huy quân đội viễn chinh Pháp: lúc này phải lấy mục tiêu chính, trên tất cả các cái khác, là cứu quân đội viễn chinh.

Đại sứ Sôven, Phó trưởng đoàn đại biểu của Pháp tại Hội nghị Giơnevơ năm 1954 về Đông Dương, trong một báo cáo gửi Bộ Ngoại giao Pháp đã viết:

``Chúng ta khó giữ được Hà Nội, Bộ chỉ huy cho biết gửi thêm hai sư đoàn nữa cũng không giữ được thủ phủ Bắc Kỳ…''. (2)

Trước đây Chính phủ Lanien muốn thương lượng trên thế mạnh quân sự để giữ nguyên được Lào, Campuchia và cố giữ được những quyền lợi gì có thể giữ được ở Việt Nam, coi đó là giải pháp ``danh dự'' đối với Pháp. Sau thất bại ở Điện Biên Phủ, chính phủ đó còn muốn thương lượng, nhằm trước hết cứu vãn đội quân viễn chinh Pháp ở Đông Dương khỏi nguy cơ bị tiêu diệt.

Phong trào phản chiến của nhân dân Pháp, mà nòng cốt là Đảng cộng sản Pháp, chống cuộc ``chiến tranh bẩn thỉu'' ở Đông Dương phát triển mạnh. Chính quyền ở Pháp vốn đã chia rẽ về nhiều vấn đề càng thêm chia rẽ trước những khó khăn nghiêm trọng về kinh tế, chính trị và xã hội. Thất bại của Pháp ở Đông Dương sẽ dẫn đến sự sụp đổ toàn bộ hệ thống thuộc địa của đế quốc Pháp, nhất là ở Bắc Phi. Trong bối cảnh đó, Pháp bước vào Hội nghị Giơnevơ năm 1954 với sự tham gia của Liên Xô, Trung Quốc, Mỹ, Anh, Pháp và các bên tham chiến ở Đông Dương.

(1) G. La-cu-tuya và P. Đơ-vi-le: Sự kết thúc của một cuộc chiến tranh, Nhà xuất bản Xơi, Paris, 1960, tr. 62-63.

(2) P. Ru-a-nê: Măng-dét Phrăng-xơ cầm quyền, Nhà cuất bản La-phông, Paris, 1965, tr. 146.



## II- LẬP TRƯỜNG CỦA TRUNG QUỐC Ở GIƠNEVƠ KHÁC HẲN LẬP TRƯỜNG CỦA VIỆT NAM, NHƯNG PHÙ HỢP VỚI LẬP TRƯỜNG CỦA PHÁP.

Nước Cộng hoà nhân dân Trung Hoa ra đời năm 1949 trong tình hình thế giới đã hình thành hai hệ thống đối lập về chính trị, kinh tế và quân sự. Ở châu Âu cuộc chiến tranh lạnh ngày càng phát triển và ở châu Á có hai cuộc chiến tranh nóng ở Triều Tiên và Đông Dương. Những người lãnh đạo nước Trung Hoa mới muốn tranh thủ điều kiện hoà bình để nhanh chóng khôi phục và phát triển kinh tế, tăng cường tiềm lực quân sự, làm cho Trung Quốc sớm trở thành một cường quốc lớn trên thế giới, thực hiện tham vọng bành trướng đại dân tộc và bá quyền nước lớn, chủ yếu hướng về Đông nam châu Á.

Mặc dù khoảng một triệu quân Trung Quốc đã bị thương vong trong chiến tranh Triều Tiên, những người lãnh đạo Trung Quốc, để có một khu đệm phía đông bắc, năm 1953 đã chịu nhận một cuộc ngừng bắn ở Triều Tiên: duy trì nguyên trạng, chia cắt lâu dài Triều Tiên.

Việt Nam và Trung Quốc là hai nước láng giềng trực tiếp, nhân dân Việt Nam và nhân dân Trung Quốc luôn luôn ủng hộ, cổ vũ, giúp đỡ lẫn nhau, vì nước Việt Nam độc lập có nghĩa là Trung Quốc không bị sự uy hiếp của chủ nghĩa đế quốc ở phía nam. Năm 1950, nước Cộng hoà nhân dân Trung Hoa công nhận và thiết lập quan hệ ngoại giao với nước Việt Nam dân chủ cộng hoà. Trung Quốc là nước viện trợ vũ khí, trang bị quân sự nhiều nhất cho Việt Nam trong những năm cuối cuộc kháng chiến chống Pháp của nhân dân Việt Nam.

Tại Hội nghị Giơnevơ năm 1954, lập trường của Việt Nam là đi tới một giải pháp hoàn chỉnh: đình chỉ chiến sự trên toàn bán đảo Đông Dương đi đôi với một giải pháp chính trị cho vấn đề Việt Nam, vấn đề Lào và  vấn đề Campuchia trên cơ sở tôn trọng độc lập, chủ quyền, thống nhất và toàn vẹn lãnh thổ của mỗi nước ở Đông Dương.

Đối với những người lãnh đạo Trung Quốc, Hội nghị Giơnevơ năm 1954 về Triều Tiên và Đông Dương là một cơ hội để họ cùng với các nước lớn bàn bạc và giải quyết các vấn đề quốc tế lớn, mặc dù Mỹ đang thù địch với Trung Quốc, Pháp chưa có quan hệ ngoại giao với Trung Quốc và Tưởng Giới Thạch còn giữ vị trí của Trung Quốc là một trong năm uỷ viên thường trực của Hội đồng bảo an Liên hợp quốc.

Những người lãnh đạo Trung Quốc muốn chấm dứt cuộc chiến tranh Đông Dương bằng một giải pháp theo kiểu Triều Tiên, nghĩa là đình chỉ chiến sự mà không có giải pháp chính trị. Ngày 24 tháng 8 năm 1953, chính thủ tướng Chu Ân Lai đã tuyên bố: đình chiến ở Triều Tiên có thể dùng làm mẫu mực cho những cuộc xung đột khác. Với một giải pháp như thế, những người cầm quyền Trung Quốc hy vọng tạo được một khu đệm ở Đông nam châu Á, ngăn chặn Mỹ vào thay thế Pháp ở Đông Dương, tránh được sự đụng đầu trực tiếp với Mỹ, bảo đảm an ninh cho biên giới phía nam của Trung Quốc, đồng thời hạn chế thắng lợi của Việt Nam, chia rẽ nhân dân ba nước Đông Dương, hòng làm suy yếu và thôn tính ba nước đó, dùng làm bàn đạp bành trướng xuống Đông nam châu Á.

Pháp đến Hội nghị Giơnevơ cũng nhằm đạt được một cuộc ngừng bắn theo kiểu Triều Tiên để cứu đội quân viễn chinh Pháp, chia cắt Việt Nam, duy trì chủ nghĩa thực dân Pháp ở Đông Dương.

Rõ ràng lập trường của Trung Quốc khác hẳn lập trường của Việt Nam, nhưng lại rất phù hợp với lập trường của Pháp.

## III- HIỆP ĐỊNH GIƠNEVƠ NĂM 1954 VỀ ĐÔNG DƯƠNG VÀ SỰ PHẢN BỘI CỦA NHỮNG NGƯỜI LÃNH ĐẠO TRUNG QUỐC.

Tháng 4 năm 1954, trong một cuộc họp giữa các đoàn đại biểu Việt Nam, Liên Xô, Trung Quốc chuẩn bị cho Hội nghị Giơnevơ về Đông Dương, đại biểu Trung Quốc đã nói : '' Nước Cộng hoà nhân dân Trung Hoa không thể công khai giúp Việt Nam được trong trường hợp cuộc xung đột ở đây mở rộng''.

Lợi dụng vị trí là một nước viện trợ quân sự chủ yếu và nắm con đường vận chuyển duy nhất chi viện cho Việt Nam, đồng thời lợi dụng việc Pháp không muốn nói chuyện trên thế yếu với Việt Nam, những người lãnh đạo Trung Quốc đã tự cho phép mình đàm phán trực tiếp với Pháp để thoả thuận về những điểm cơ bản của một giải pháp về vấn đề Đông Dương.

Quá trình đàm phán về thực chất tại Hội nghị Giơnevơ đã diễn ra qua hai thời kỳ:

Thời kỳ thứ nhất từ ngày 8 tháng 5 đến ngày 23 tháng 6  năm 1954, trưởng đoàn đại biểu Pháp, trong khi tránh tiếp xúc với Việt Nam, đã đàm phán trực tiếp với trưởng đoàn đại biểu Trung Quốc bốn lần, đi tới thoả thuận về những nét cơ bản của một Hiệp định ngừng bắn ở Đông Dương.

Đáng chú ý là cuộc tiếp xúc lần thứ ba ngày 17 tháng 6 năm 1954, thủ tướng Chu Ân Lai gặp trưởng đoàn đại biểu Pháp G.Biđô, đưa ra những nhân nhượng chính trị có tính chất cơ bản, có hại cho nhân dân ba nước Việt Nam, Lào và Campuchia: Trung Quốc có thể chấp nhận Việt Nam có hai chính quyền (Chính phủ Việt Nam dân chủ cộng hoà và Chính phủ bù nhìn Bảo Đại), công nhận Chính phủ Vương quốc Lào và Chính phủ Vương quốc Campuchía, từ bỏ yêu cầu có đại biểu của Cính phủ kháng chiến Lào và Chính phủ kháng chiến Campuchia tham gia Hội nghị Giơnevơ, và đưa ra vấn đề quân đội nước ngoài, kể cả quân tình nguyện Việt Nam phải rút khỏi Lào và Campuchia.

Lần thứ tư, ngày 23 tháng 6 năm 1954, thủ tướng Chu Ân Lai gặp Măngđét Phranxơ, thủ tướng mới của Pháp, đưa ra những nhượng bộ mới: chia cắt Việt Nam, hai miền Việt Nam cùng tồn tại hoà bình, giải quyết vấn đề quân sự trước, tách rời việc giải quyết ba vấn đề Việt Nam, Lào, Campuchia; Trung Quốc sẵn sàng nhìn nhận ba nước này trong khối Liên hiệp Pháp và muốn Lào, Campuchia sẽ có bộ mặt mới ở Đông nam châu Á như Ấn Độ, Inđônêxia, ngược lại chỉ yêu cầu không có căn cứ quân sự của Mỹ ở Đông Dương. Do đó, Trung Quốc và Pháp đạt được một giải pháp khung cho vấn đề Đông Dương.

Những điểm mà những người lãnh đạo Trung Quốc đã thoả thuận với Pháp rất phù hợp với giải pháp 7 điểm của Anh-Mỹ đưa ra ngày 29 tháng 6 năm 1954, tức là 6 ngày sau cuộc tiếp xúc giữa Chu Ân Lai và Măngđét Phranxơ.

Thời kỳ thứ hai từ ngày 23 tháng 6 đến ngày 20 tháng 7 năm 1954, Đoàn đại biểu Pháp tiến hành đàm phán trực tiếp với Đoàn đại biểu Việt Nam để giải quyết các vấn đề cụ thể. Trung Quốc giữ vai trò thúc đẩy phía Việt Nam nhân nhượng. Đến ngày 10 tháng 7 năm 1954, phía Việt Nam vẫn kiên trì lập trường của mình về vấn đề Việt Nam, Lào và Campuchia, vẫn chủ trương đòi có đại biểu của Chính phủ kháng chiến Lào và Chính phủ kháng chiến Campuchia tham gia như các bên đàm phán, định giới tuyến quân sự tạm thời ở Việt Nam là vĩ tuyến 13, tổ chức tổng tuyển cử tự do trong thời hạn 6 tháng để thống nhất nước nhà. Đối với việc giải quyết vấn đề Lào và vấn đề Campuchia, Việt Nam vẫn giữ quan điểm là ở Lào có hai vùng tập kết của lực lượng kháng chiến: một vùng ở phía bắc giáp Trung Quốc và Việt Nam và một vùng ở Trung và Hạ Lào; ở Campuchia có hai vùng tập kết của lực lượng kháng chiến Campuchia ở phía đông và đông bắc sông Mêcông và phía tây nam sông Mêcông; tổ chức tổng tuyển cử tự do trong thời hạn 6 tháng ở Lào và Campuchia.

Từ tháng 5 năm 1954, đoàn đại biểu Trung Quốc đưa ra phương án lấy vĩ tuyến 16 làm giới tuyến giữa hai miền Việt Nam, và còn muốn Việt nam nhân nhượng nhiều hơn nữa, thậm chí muốn Việt Nam bỏ cả thủ đô Hà Nội, thành phố Hải Phòng và đường số 5 (đường nối liền Hà Nội với Hải Phòng):

``Đánh giá (phương án vĩ tuyến 16) khó có thể thoả thuận, nếu không được thì sẽ lấy Hải Phòng làm cửa bể tự do, ở gần đấy cho Pháp đóng một số quân nhất định, nếu không được nữa thì đem đường số 5 và Hà Nội, Hải Phòng làm khu công quản và phi quân sự…''. (3)

Nhưng về sau, đặc biệt từ ngày 10 tháng 7 năm 1954, 10 ngày trước khi Hội nghị Giơnevơ kết thúc, Trung Quốc ngày càng thúc ép Việt Nam nhân nhượng, ``có những điều kiện công bằng và hợp lý để Chính phủ Pháp có thể nhận được để đi đến Hiệp định trong vòng 10 ngày, điều kiện đưa ra nên giản đơn, rõ ràng để dễ đi đến hiệp thương, không nên làm phức tạp lôi thôi để tránh thảo luận mất thì giờ, rườm rà, kéo dài đàm phán để cho Mỹ phá hoại''. (4)

Khi đó Trung Quốc còn lo sợ Mỹ có thể can thiệp bằng vũ trang vào Đông Dương, uy hiếp an ninh của Trung Quốc, nhưng cần nói rằng Trung Quốc cũng dùng những lời của Mỹ đe doạ chiến tranh để ép Việt Nam.

(3) Điện văn của Chu Ân Lai, ngày 30 tháng 5 năm 1954 gửi Ban Chấp Hành trung ương Đảng Cộng Sản Trung Quốc, sao gửi Ban Chấp Hành trung ương Đảng Lao Động Việt Nam.

(4) Điện văn của Chu Ân Lai, ngày 10 tháng 7 năm 1954 gửi Ban chấp hành trung ương Đảng Lao Động Việt Nam.



Sự thật là sau cuộc chiến tranh Triều Tiên, Mỹ không có khả năng can thiệp quân sự trực tiếp vào Đông Dương. Thái độ cứng rắn của Mỹ ở Hội nghị Giơnevơ chẳng qua là do Mỹ sợ  Pháp vì bị thua ở chiến trường, có nhiều khó khăn về chính trị, kinh tế, tài chính, có thể chấp nhận một giải pháp không có lợi cho việc Mỹ nhảy vào Đông Dương sau này. Khi Pháp đã cùng với Trung Quốc thoả thuận được một giải pháp khung về Đông Dương và Mỹ đã đưa được tên tay sai Ngô Đình Diệm về làm thủ tướng chính phủ bù nhìn Sài Gòn ( ngày 13 tháng 6 năm 1954), thì Mỹ thấy có thể chấp nhận một hiệp định theo hướng Trung Quốc và Pháp đã thoả thuận giải quyết cả ba vấn đề Việt Nam, Lào và Campuchia. Tuy vậy, Mỹ không tham gia vào bản Tuyên bố cuối cùng của Hội nghị là vì Mỹ muốn được rảnh tay sau này để vi phạm Hiệp định Giơnevơ thông qua chính quyền Ngô Đình Diệm, buộc Pháp phải rút lui để thay thế Pháp ở Đông Dương.

[anh4]

Sau Điện Biên Phủ, rõ ràng là với sự giúp đỡ của hệ thống xã hội chủ nghĩa, nhất là của Trung Quốc, quân và dân Việt Nam có khả năng giải phóng cả nước, nhưng giải pháp mà Đoàn đại biểu Trung Quốc đã thoả thuận với Đoàn đại biểu Pháp ở Giơnevơ không phản ảnh so sánh lực lượng trên chién trường, cũng không đáp ứng đầy đủ những yêu cầu chính trị của giải pháp do Đoàn đại biểu Việt Nam đề ra.

Xuất phát từ truyền thống yêu chuộng hoà bình, theo xu thế chung giải quyết các vấn đề tranh chấp bằng thương lượng và trong tình thế bị Trung Quốc ép buộc, Việt Nam đã chấp nhận giải pháp: các nước tôn trọng các quyền dân tộc cơ bản của nhân dân Việt Nam, nhân dân Lào, nhân dân Campuchia, ngừng bắn đồng thời ở Việt Nam và trên toàn chiến trường Đông Dương, Pháp rút quân, vĩ tuyến 17 là giới tuyến quân sự tạm thời chia Việt Nam làm hai miền, tiến tới có tổng tuyển cử tự do trong cả nước sau hai năm để thống nhất nước nhà.

Ở Lào có một khu tập kết cho các lực lượng kháng chiến lào gồm hai tỉnh Sầm Nưa và Phongsalỳ. Ở Campuchia lực lượng kháng chiến không có khu tập kết nào và phục viên tại chỗ.

Chiến thắng Điện Biên Phủ và Hiệp định Giơnevơ năm 1954 đánh dấu một bước thắng lợi của các lực lượng cách mạng ở Đông Dương, đồng thời là một đóng góp quan trọng mở đầu sự tan rã hoàn toàn của hệ thống thuộc địa của đế quốc Pháp và báo hiệu quá trình sụp đổ không thể đảo ngược được của chủ nghĩa thực dân cũ, của chủ nghĩa đế quốc thế giới. Nhưng giải pháp Giơnevơ đã ngăn cản nhân dân ba nước Việt Nam, Lào và Campuchia đạt được thắng lợi hoàn toàn trong cuộc kháng chiến chống Pháp, một khả năng rõ ràng là hiện thực như so sánh lực lượng trên chiến trường lúc bấy giờ đã chỉ rõ.

Đó là điều mà những người lãnh đạo Trung Quốc hiểu rõ hơn ai hết.

Đây là sự phản bội thứ nhất  của những người lãnh đạo Trung Quốc đối với cuộc đấu tranh cách mạng của nhân dân Việt Nam cũng như nhân dân Lào và  nhân dân Campuchia.

# PHẦN THỨ BA

TRUNG QUỐC VỚI CUỘC ĐẤU TRANH CỦA NHÂN DÂN VIỆT NAM ĐỂ GIẢI PHÒNG MIỀN NAM, THỐNG NHẤT NƯỚC NHÀ (1954-1975)
===

Bất chấp Hiệp định Giơnevơ năm 1954 về Việt Nam, đế quốc Mỹ nhảy vào miền Nam Việt Nam, nhằm tiêu diệt phong trào yêu nước của nhân dân Việt Nam, thôn tính miền Nam, chia cắt lâu dài nước Việt Nam, biến miền Nam Việt Nam thành thộc địa kiểu mới và căn cứ quân sự của Mỹ, lập phòng tuyến ngăn chặn chủ nghĩa xã hội lan xuống Đông Nam châu Á, đồng thời lấy miền Nam làm căn cứ để tiến công miền bắc, tiền đồn của hệ thống xã hội chủ nghĩa ở Đông Nam châu Á, đè bẹp và đẩy lùi chủ nghĩa xã hội ở khu vực này, hòng bao vây uy hiếp các nước xã hội chủ nghĩa khác. Thất bại trong việc dùng chính quyền tay sai Ngô Đình Diệm và tiến hành ``chiến tranh đặc biệt'' ở miền Nam Việt Nam, đế quốc Mỹ đã lao vào cuộc phiêu liêu quân sự chống nhân dân Việt Nam, tiến hành một cuộc chiến tranh xâm lược quy mô lớn nhất, dài ngày nhất, ác liệt và dã man nhất từ sau chiến tranh thế giới thứ hai.

Hưởng ứng lời kêu gọi bất hủ của Chủ tịch Hồ Chí Minh: ``Không có gì quý hơn độc lập tự do'' nhân dân Việt Nam đã nhất tề đứng lên chống Mỹ, giương cao ngọn cờ độc lập dân tộc và chủ nghĩa xã hội, kết hợp sức mạnh của dân tộc với sức mạnh của thời đại, sức mạnh trong nước với sức mạnh quốc tế tạo thành một sức mạnh tổng hợp to lớn để đánh thắng bọn xâm lược Mỹ. Trong chiến tranh, nhân dân Việt Nanm luôn luôn thực hành chiến lược tiến công, đẩy mạnh đấu tranh trên cả ba mặt trận quân sự, chính trị và ngoại giao, đồng thời biết kéo địch xuống thang, giành thắng lợi từng bước, tiến lên giành thắng lợi hoàn toàn.

Quá trình hơn 20 năm nhân dân Việt Nam chống chính sách can thiệp và xâm lược của đế quốc Mỹ cũng là quá trình đấu tranh bền bỉ, hết sức phức tạp chống những âm mưu và hành động khi kín đáo, lúc trắng trợn của những người cầm quyền Trung Quốc thoả hiệp và câu kết với đế quốc Mỹ nhằm kiềm chế và làm suy yếu cách mạng Việt Nam, hòng khuất phục Việt Nam, từng bước bành trướng ở Đông Dương và Đông Nam Châu Á.

## I- THỜI KỲ 1954-1964 - NHỮNG NGƯỜI CẦM QUYỀN TRUNG QUỐC NGĂN CẢN NHÂN DÂN VIỆT NAM ĐẤU TRANH ĐỂ THỰC HIỆN THỐNG NHẤT NƯỚC NHÀ.

Sau khi cùng với đế quốc thoả hiệp trong giải pháp Giơnevơ năm 1954, tạo được khu đệm an toàn ở phía nam, những người lãnh đạo Trung Quốc yên tâm thúc đẩy việc hoàn thành kế hoạch 5 năm lần thứ nhất (1953-1957), và từ năm 1958 đề ra kế hoạch ``đại nhảy vọt''  với tham vọng đuổi kịp và vượt một số cường quốc về kinh tế trong một thời gian ngắn và ra sức xây dựng lực lượng hạt nhân.

Về đối ngoại họ đi vào con đường hoà hoãn với đế quốc Mỹ, tiến hành những cuộc nói chuyện với Mỹ ở Giơnevơ từ tháng 8 năm 1955; đồng thời tìm cách mở rộng ảnh hưởng ở châu Á, nhất là ở Đông Nam châu Á và Nam Á.

Xuất phát từ đường lối đối nội, đối ngoại đó, những người cầm quyền Bắc Kinh đã hành động ngược lại lợi ích của nhân dân Việt Nam, phù hợp với lợi ích của đế quốc Mỹ ở Việt Nam nói riêng và Đông Dương nói chung.

### 1- Gây sức ép để Việt Nam chấp nhận chủ trương ``trường kỳ mai phục''

Đế quốc Mỹ và tay sai trắng trợn phá hoại việc tổ chức tổng tuyển cử để thống nhất nước Việt Nam trong thời hạn hai năm như Hiệp định Giơnevơ về Việt Nam đã quy định, đồng thời đàn áp cực kỳ tàn bạo phong trào yêu nước ở miền Nam Việt Nam.

Những người cầm quyền Bắc Kinh luôn luôn ``thuyết phục'' Việt Nam rằng công cuộc thống nhất là ``một cuộc đấu tranh trường kỳ'' và không thể thực hiện được bằng lực lượng vũ trang.
Tháng 11 năm 1956, chủ tịch Mao Trạch Đông nói với những người lãnh đạo Việt Nam:
``Tình trạng nước Việt Nam bị chia cắt không thể giải quyết được trong một thời gian ngắn mà cần phải trường kỳ… nếu 10 năm chưa được thì phải 100 năm''

Diễn biến thực tế của cuộc kháng chiến chống Mỹ, cứu nước của nhân dân Việt Nam đã chứng tỏ cuộc kháng chiến đó có lâu dài, nhưng không phải lâu dài vô hạn độ như chủ tịch Mao Trạch Đông nói.

Tháng 7 năm 1955, Tổng bí thư Đảng cộng sản Trung Quốc Đặng Tiểu Bình doạ:

``Dùng lực lượng vũ trang để thống nhất nước nhà sẽ có hai khả năng: một là thắng và một khả năng nữa là mất cả miền bắc''.

Tháng 7 năm 1957, chủ tịch Mao Trạch Đông lại nói:

``Vấn đề là phải giữ biên giới hiện có. Phải giữ vĩ tuyến 17…Thời gian có lẽ dài đấy. Tôi mong thời gian dài thì sẽ tốt.''

Đó là một điều trái hẳn với Hiệp nghị Giơnevơ, vì theo Hiệp nghị này, vĩ tuyến 17 không phải là biên giới giữa hai quốc gia mà chỉ là giưói tuyến quân sự tạm thời giữa hai miền Việt Nam. Những người lãnh đạo Bắc Kinh nhiều lần nhấn mạnh rằng ở miền Nam Việt Nam ``chỉ có thể dùng phương châm thích hợp là trường kỳ mai phục, tích trữ lực lượng, liên hệ quần chúng, chờ đợi thời cơ''. Thực chất phương châm đó là gì?

Trong một cuộc trao đổi ý kiến với những người lãnh đạo Việt Nam, uỷ viên trung ương của Đảng cộng sản Trung Quốc, thứ trưởng Bộ ngoại giao Trương Văn Thiên cho rằng ở miền Nam Việt Nam có thể tiến hành đánh du kích. Nhưng sau đó, đại sứ Trung Quốc tại Hà Nội, theo chỉ thị của Bắc Kinh, đã thông báo với phía Việt Nam rằng đó không phải là ý kiến của Trung ương đảng cộng sản Trung Quốc mà chỉ là ý kiến cá nhân.

Như vậy, ``trường kỳ mai phục'' có nghĩa là nhân dân Việt Nam thủ tiêu đấu tranh cách mạng, để mặc cho Mỹ Diệm thả sức đàn áp nhân dân Việt Nam.

``Giữ vĩ tuyến 17'', ``trường kỳ mai phục'', ``tích trữ lực lượng'', ``chờ đợi thời cơ''… đó chẳng qua là luận điệu quanh co nhằm che dấu ý đồ của Bắc Kinh muốn duy trì nguyên trạng chính trị ở Việt Nam, công nhận Chính phủ Việt Nam dân chủ cộng hoà và nguỵ quyền Sài Gòn song song tồn tại. Ngày 22 tháng 7 năm 1954, khi ăn cơm với Ngô Đình Luyện, em ruột của Ngô Đình Diệm tại Giơnevơ, thủ tướng Chu Ân Lai đã gợi ý đặt một công sứ quán của Sài Gòn tại Bắc Kinh. Dù Ngô Đình Diệm đã bác bỏ gợi ý đó, nhưng đây là một bằng chứng rõ ràng là chỉ 24 giờ sau khi Hiệp định Giơnevơ được ký kết, những người lãnh đạo Bắc Kinh đã lộ rõ ý họ muốn chia cắt lâu dài nước Việt Nam.

Trong thời gian từ năm 1954 đến năm 1959, với chính sách đàn áp phát xít, Mỹ Diệm đã giết hại hàng vạn người Việt Nam yêu nước, lùa hàng vạn nhân dân vào các trại tập trung trá hình, gây cho nhân dân Việt Nam những tổn thất to lớn so với bất kỳ thời gian nào khác trong cuộc chiến tranh cứu nước của mình. Nếu cứ để chúng tiếp tục giết hại những người Việt Nam yêu nước thì làm sao ``tích trữ'' được lực lượng, còn nhân dân đâu để : '' liên hệ quần chúng'' và chờ đợi đến thời cơ nào? Theo cái đà đó thì nước Việt Nam sẽ mất độc lập và vĩnh viễn bị chia cắt.

Vấn đề có ý nghĩa chiến lược đối với cách mạng miền Nam là tiếp tục đấu tranh chính trị hay kết hợp cả đấu tranh chính trị và đấu tranh vũ trang?

Nhân dân Việt Nam kiên quyết đi theo đường lối độc lập, tự chủ của mình. Cuối năm 1959 đầu năm 1960, nhân dân trong những vùng rộng lớn thuộc đồng bằng Nam Bộ và miền Nam Trung Bộ đã nhất tề ``đồng khởi'' kết hợp đấu tranh chính trị với đấu tranh quân sự, làm rung chuyển tận gốc chế độ tay sai Ngô Đình Diệm.

### 2- Ngăn cản nhân dân Việt Nam đẩy mạnh đấu tranh vũ trang ở miền Nam.

Các cuộc đồng khởi, thực chất là những cuộc khởi nghĩa thừng phần đã nhanh chóng phát triển thành một cuộc chiến tranh cách mạng, vừa đấu tranh quân sự vừa đấu tranh chính trị chống lại `` chiến tranh đặc biệt'' của đế quốc Mỹ. Nhưng những người cầm quyền Trung Quốc đã không đồng tình với chủ trương đó của Việt Nam.

Tháng 5 năm 1960, hội đàm với phía Việt Nam, họ nói về miền Nam Việt Nam như sau:
`` Không nên nói đấu tranh chính trị hay đấu tranh quân sự là chính…Đấu tranh chính trị hay đấu tranh quân sự không phải là cướp chính quyền ngay, mà cuộc đấu tranh vẫn là trường kỳ...Dù Diệm có đổ cũng không thể thống nhất ngay được, vì đế quốc mỹ không chịu để như vậy đâu…

Miền bắc có thể ủng hộ về chính trị cho miền Nam, giúp miền Nam đề ra các chính sách nhưng chủ yếu là bồi dưỡng tinh thần tự lực cánh sinh cho anh em miền Nam…Khi ăn chắc, miền bắc có thể giúp quân sự cho miền Nam, nghĩa là khi hoàn toàn chắc chắn không xảy ra chuyện gì, có thể cung cấp một số vũ trang mà không cho ai biết. Nhưng nói chung là không giúp.''

Như vậy, khi không cản được nhân dân miền Nam Việt Nam ``đồng khởi'' thì họ cho rằng hình thức chiến đấu ở miền Nam là đánh du kích, đánh nhỏ từng đơn vị trung đội, đại đội.
Làm chủ vận mệnh của mình, nhân dân Việt Nam đã đưa cuộc chiến tranh cách mạng ở miền Nam tiến lên vững mạnh. Cuối năm 1963, chế độ độc tài phát xít Ngô Đình Diệm sụp đổ, ``chiến tranh đặc biệt'' của Mỹ phá sản hoàn toàn.

### 3- Lôi kéo Việt Nam chống Liên Xô

Đầu những năm  1960, trong khi ngăn cản nhân dân Việt Nam đẩy mạnh chiến đấu chống Mỹ, những người lãnh đạo Bắc Kinh giơ cao cùng một lúc hai chiêu bài chống đế quốc Mỹ và chống Liên Xô, nhưng trên thực tế họ tiếp tục hoà hoãn với đế quốc Mỹ ở châu Á, đánh lạc hướng cuộc đấu tranh chống Mỹ của nhân dân thế giới để thực hiện mưu đồ chống Liên Xô, xoá bỏ hệ thống xã hội chủ nghĩa, nắm quyền ``lãnh đạo cách mạng thế giới'', chuẩn bị tích cực cho việc hoà hoãn và câu kết với đế quốc Mỹ.

Trong những cuộc hội đàm với phía Việt Nam năm 1963, họ tìm cách thuyết phục Việt Nam chấp nhận quan điểm của họ là phủ nhận hệ thống xã hội chủ nghĩa và mở cho họ ``một con đường'' xuống Đông Nam châu Á. Cũng trong năm 1963, những người lãnh đạo Trung Quốc đưa ra cái gọi là Cương lĩnh 25 điểm về đường lối chung của phong trào cộng sản quốc tế và đề nghị triệu tập cái gọi là hội nghị 11 đảng cộng sản, thực tế là để nắm vai trò ``lãnh đạo cách mạng thế giới'' và lập một ``Quốc tế cộng sản'' mới do Bắc Kinh khống chế. Họ thiết tha yêu cầu Việt Nam đồng tình là cốt lợi dụng uy tín và vai trò của Việt Nam trong phong trào cộng sản và phong trào giải phóng dân tộc trên thế giới. Nhằm mục đích này, họ còn hứa hẹn viện trợ ồ ạt để lôi kéo Việt Nam. Tổng bí thư đảng cộng sản Trung Quốc Đặng Tiểu Bình thông báo với lãnh đạo Viẹt Nam ý kiến của lãnh đạo Trung Quốc sẽ viên trợ 1 ti nhân dân tệ nếu Việt Nam khước từ mọi viện trợ của Liên Xô.

Phía Việt Nam đã khẳng định thái độ kiên quyết bảo vệ hệ thống xã hội chủ nghĩa, không tán thành việc họp hội nghị 11 đảng và không để những người lãnh đạo Bắc Kinh dùng đất nước Việt Nam làm bàn đạp cho mưu đồ bành trướng của họ. Do tahí độ kiên quyết của phía Việt Nam, Cương lĩnh 25 điểm không gây được tiếng vang, âm mưu lập ``Quốc tế cộng sản'' mới cũng không thành.



◎◎◎

Trong thời kỳ này, đối với cách mạng Lào, những người cầm quyền Trung Quốc cũng thi hành một chính sách giống như đối với Việt Nam. Họ cũng gây sức ép để lực lượng cách mạng Lào `` trường kỳ mai phục''. Khi Mỹ và tay sai lật đổ Chính phủ Lào liên hiệp, tiến hành ``chiến tranh đặc biệt'', họ ngăn cản cách mạng Lào đấu tranh vũ trang kết hợp với đấu tranh chính trị và mưu toan ép Đảng Nhân dân cách mạng Lào ``sớm lập lại Chính phủ liên hiệp'', sợ rằng đốm lửa Sầm Nưa, Phongsalỳ có thể lan ra cả Việt Nam và miền Nam Trung Quốc.

Trong một cuộc hội đàm với phía Việt Nam tháng 8 năm 1961, khi đề cập đến vấn đề Lào, phía Trung Quốc nói:

``Cần hết sức tránh việc trực tiếp tham gia chiến tranh. Nếu Mỹ trực tiếp vào Lào thì miền bắc Việt Nam, Vân Nam, Quảng Tây sẽ xảy ra vấn đề gì? Cần tính đến việc phiêu ău của Mỹ''.
Đối với việc giải quyết vấn đề Lào tại Giơnevơ năm 1961-1962, họ còn chủ trương chia nước Lào theo chiều ngang thành hai vùng: vùng giải phóng ở phía bắc, vùng do nguỵ quyền Viêng Chăn kiểm soát ở phía nam. Đó là một âm mưu thâm độc nhằm buộc lực lượng cách mạng Lào phải lệ thuộc vào Trung Quốc và cô lập cách mạng miền Nam Việt Nam.

Nhưng những nhà lãnh đạo cách mạng Lào kiên quyết giữ vững đường lối riêng của mình, lực lượng kháng chiến Lào ngày càng giành nhiều thắng lợi buộc đế quốc Mỹ và tay sai phải ký Hiệp nghị Giơnevơ năm 1962 công nhận nền trung lập của Lào và nhận có đại biểu của Mặt trận Lào yêu nước trong Chính phủ liên hiệp thứ hai ở Lào.

## II -THỜI KỲ 1965-1969: LÀM YẾU VÀ KÉO DÀI CUỘC KHÁNG CHIẾN CỦA NHÂN DÂN VIỆT NAM

Trong thời kỳ này ở Trung Quốc diễn ra cái gọi là cuộc ``đại cách mạng văn hoá '', thực chất là một cuộc đấu tranh nội bộ điên cuồng và đẫm máu làm đảo lộn toàn bộ xã hội Trung Quốc, nhằm xoá bỏ chủ nghĩa Mác Lênin, phá vỡ Đảng cộng sản Trung Quốc và cơ cấu nhà nước, khôi phục vị trí độc quyền lãnh đạo của chủ tịch Mao Trạch Đông và đường lối Mao Trạch Đông ở trong nước, chống Liên Xô, phá hoại cách mạng thế giới, câu kết với đế quốc Mỹ ở ngoài nước, đẩy mạnh việc thực hiện chính sách bành trướng đại dân tộc và bá quyền nước lớn.

Những người lãnh đạo Trung Quốc, vì lợi ích chiến lược của họ trong thời kỳ này, đã dấn sâu vào con đường phản bội nhân dân Việt Nam.

### 1- Bật đèn xanh cho Mỹ trực tiếp xâm lược Việt Nam

Sau cuộc chiến tranh ở Triều Tiên, đế quốc Mỹ đã rút ra bài học là không nên tiến hành một cuộc chiến tranh trên lục địa châu Á, nhất là ở những nước láng giềng của Trung Quốc để tránh đụng độ quân sự trực tiếp với Trung Quốc. Nhưng hơn 10 năm sau, đế quốc Mỹ lại liều lĩnh tiến hành một cuộc phiêu kưu quân sự ở  Việt Nam sau khi gây ra cái gọi là `` sự kiện vịnh Bắc Bộ'' tháng 8 năm 1964. Một trong những nguyên nhân chính là đế quốc Mỹ đã được yên tâm về phía những người cầm quyền Trung Quốc.

Tháng 1 năm 1965, qua nhà báo Mỹ Étga Xnâu, chủ tịch Mao Trạch Đông nhắn Oasinhtơn:

``Quân đội Trung Quốc sẽ không vượt biên giới của mình để đánh nhau. Đó là điều hoàn toàn rõ ràng. Chỉ khi nào Mỹ tấn công, người Trung Quốc mới chiến đấu. Phải chăng như vậy là không rõ ràng? Người Trung Quốc rất bận về công việc nội bộ của mình. Đánh nhau ngoài biên giới nước mình là phạm tội ác. Tại sao người Trung Quốc phải làm như vậy? Người Nam Việt Nam có thể đương đầu với tình hình.'' (Ét-ga Xnâu: Cuộc cách mạng lâu dài, Nhà xuất bản Hớt xin xơn, Lân đơn, 1973, tr. 216)

Sau đó bằng nhiều cách, kể cả bằng cách trực tiếp nói trong cuộc đàm phán Trung Mỹ cấp đại sứ tại Vacsava, phía Trung Quốc làm cho Mỹ hiểu rõ câu nói của chủ tịch Mao Trạch Đông và yên tâm rằng: ``người không đụng đến ta thì ta không đụng đến người''.

Do đó từ tháng 2 năm 1965 chính quyền Giônxơn đã sử dụng bộ máy chiến tranh khổng lồ của Mỹ vào chiến trường Việt Nam, đưa quân Mỹ vào miền Nam tiến hành chiến tranh cục bộ, đồng thời làm chiến tranh phá hoại bằng không quân chống nước Việt Nam dân chủ cộng hoà, gây nên biết bao tang tóc và tàn phá đối với nhân dân cả nước Việt Nam.

Như vậy, những người cầm quyền Trung Quốc đã bộc lộ những tính toán lắt léo, những ý đồ thâm độc của họ. Họ đẩy Mỹ sa lầy trong chiến tranh xâm lược Việt Nam để họ yên tâm làm cuộc ``cách mạng văn hoá''. Khi Mỹ lao vào cuộc phiêu lưu quân sự ở Việt Nam, họ muốn làm yếu cả Mỹ lẫn Việt Nam.

Câu nói của thủ tướng Chu Ân Lai với tổng thống Ai Cập A. Nátxe ngày 23 tháng 6 năm 1965, do ông Môhamét Hátxenen Hâycan, người bạn thân thiết và cố vấn riêng của tổng thống A. Nátxe kể lại là một bằng chứng hùng hồn:

``Mỹ càng đưa nhiều quân vào Việt Nam thì chúng tôi càng vui lòng vì chúng tôi biết rằng chúng tôi nắm chúng trong tay, chúng tôi có thể lấy máu chúng. Nếu Ngài muốn giúp đỡ người Việt Nam thì cần khuyến khích Mỹ ném càng nhiều lính Mỹ vào Việt Nam càng tốt'' (Mô-ha-mét Hát-xe-nen Hây-can: "Những tài liệu từ Cai-rô" Nhà xuất bản Phia-ma-ri-ông, Paris, 1972, tr. 238)

Đối với nhân dân Việt Nam, những người cầm quyền Trung Quốc chỉ một mực nhấn mạnh là phải đánh lâu dài, đánh du kích, không đánh lớn. Họ giúp nhân dân Việt Nam chủ yếu là  gúp vũ khí nhẹ, đạn dược, trang bị hậu cần. Họ không muốn cuộc chiến tranh Việt Nam sớm kết thúc vì không những họ muốn lực lượng cách mạng Việt Nam suy yếu mà còn muốn lợi dụng càng lâu càng tốt cái tiếng ``viện trợ Việt Nam'' để giương cao ngọn cờ ``cách mạng trệt để'', tập họp lực lượng ở châu Á, châu Phi, châu Mỹ latinh, đẩy mạnh chiến dịch chống Liên Xô.

Không có gì đáng ngạc nhiên khi họ khước từ thi hành một hiệp định quân sự bí mật giữa Việt Nam và Trung Quốc. Theo hiệp định này, về nguyên tắc, đến tháng 6 năm 1965 phía Trung Quốc phải gửi phi công sang giúp Việt Nam. Nhưng ngày 16 tháng 7 năm 1965 Bộ Tổng tham mưu Quân giải phóng Trung Quốc đã báo cho Bộ Tổng tham mưu Quân đội nhân dân Việt Nam là phía Trung Quốc không thể gửi phi công sang Việt Nam được vì ``thời cơ chưa thích hợp'' và ``làm như vậy không ngăn cản được địch đẩy mạnh oanh tạc ``. Trong một cuộc hội đàm với  phía Việt Nam tháng 8 năm 1966 họ cũng nói: `` Trung Quốc không đủ khả năng về không quân giúp bảo vệ Hà Nội''.

### 2 -Phá hoại mọi hành động thống nhất ủng hộ Việt Nam chống Mỹ xâm lược.

Để Việt Nam buộc phải lệ thuộc vào Trung Quốc, những người cầm quyền Bắc Kinh ra sức ngăn cản mọi hành động thống nhất của các lực lượng cách mạng và tiến bộ trên thế giới ủng hộ Việt Nam chống Mỹ.

Ngày 28 tháng 2 năm 1965, họ bác bỏ dự thảo do phía Việt Nam đề nghị ngày 22 tháng 5 năm 1965 là các nước xã hội chủ nghĩa ra tuyên bố chung lên án Mỹ tăng cường chiến tranh xâm lược chống nước Việt Nam dân chủ cộng hoà.

Tháng 3 năm 1965, họ lại hai lần bác bỏ đề nghị của Liên Xô về vấn đề thống nhất hành động để bảo đảm an ninh của Việt Nam dân chủ cộng hoà. Chính vì thế họ đã bác bỏ đề nghị của Liên Xô lập cầu hàng không qua Trung Quốc, lập các sân bay trên đất Trung Quốc để bảo vệ Việt Nam dân chủ cộng hoà.

Tháng 2 năm 1966, chủ tịch Mao Trạch Đông bác bỏ việc thành lập Mặt trận quốc tế ủng hộ Việt Nam đã được nêu trong cuộc hội đàm cấp cao Việt Trung.

Tháng 3 năm 1966, cũng chủ tịch Mao Trạch Đông bác bỏ việc thành lập Mặt trận quốc tế thống nhất ủng hộ Việt Nam do Đảng cộng sản Nhật Bản nêu trong cuộc hội đàm cấp cao  với Đảng cộng sản Trung Quốc. Sau đó bọn tay sai của Bắc Kinh đã trắng trợn hành hung đại diện Đảng cộng sản Nhật Bản tại Trung Quốc.

Nhưng những người cầm quyền Trung Quốc lại muốn thành lập một cái gọi là Mặt trận nhân dân thế giới do họ khống chế:

``Cần phải thành lập Mặt trận thống nhất thế giới rộng rãi nhất chống đế quốc Mỹ và tay sai…Tất nhiên Mặt trận đó không thể bao gồm họ (Liên Xô) được…'' (Nghị quyết Hội nghị lần thứ 11 khóa 8 Ban chấp hành trung ương Đảng Cộng Sản Trung Quốc, tháng 8 năm 1956).

Đi đôi với việc phá hoại mọi hành động thống nhất ủng hộ Việt Nam, họ gây khó khăn rất lớn cho việc vận chuyển hàng viện trợ của Liên Xô và các nước xã hội chủ nghĩa khác quá cảnh đất Trung Quốc và tìm cách điều chỉnh sự viện trợ đó để hạn chế khả năng đánh lớn của nhân dân Việt Nam, đặc biệt trong các mùa khô.

Trong cuộc đấu tranh chống kẻ thù chung là chủ nghĩa thực dân và chủ nghĩa đế quốc, nhân dân Việt Nam, nhân dân Lào và nhân dân Campuchia đã đoàn kết chặt chẽ với nhau và đó là truyền thống, là một nhân tố thắng lợi của nhân dân ba nước. Sau khi Mỹ tăng cường chiến tranh chống nhân dân Việt Nam và uy hiếp độc lập, hoà bình và trung lập của Lào và Campuchia, tháng 3 năm 1965 Hội nghị nhân dân các nước Đông Dương đã họp ở Phnôm Pênh, Mặt trận đoàn kết chống Mỹ của nhân dân các nước Đông Dương được hình thành. Những người cầm quyền Trung Quốc bên ngoài buộc phải hoan nghên kết quả của hội nghị, nhưng bên trong mưu tính phá hoại Mặt trận đoàn kết nhân dân ba nước Đông Dương. Theo chính sách cổ truyền ``chia để trị'' của tất cả bọn đế quốc và phản động, họ chia rẽ ba nước Đông Dương, làm suy yếu và cô lập Việt Nam hòng đánh tỉa từng nước.

Năm 1966 ở Lào, trong các vùng giải phóng do Mặt trận Lào yêu nước quản lý, theo kế hoạch của Bắc Kinh, người Hoa tìm cách chia rẽ người Lào với người Việt Nam, tuyên truyền xuyên tạc và xúi giục một số phần tử chống lại Đảng nhân dân cách mạng Lào. Mặt khác, Bắc Kinh tranh thủ chính quyền Vương quốc Lào, đẩy mạnh việc giúp Lào làm hệ thống đường ở 5 tỉnh Bắc Lào nối liền với cao nguyên Vân Nam của Trung Quốc và có những nhánh đi về hướng Việt Nam, Thái Lan nhằm tạo điều kiện can thiệp vào công việc nội bộ của Lào và chuẩn bị cho những kế hoạch bành trướng sau này.

Ở Campuchia, từ trước năm 1965, những người cầm quyền Trung Quốc đã trắng trợn vu cáo Việt Nam hy sinh lợi ích của cách mạng Campuchia tại Hội nghị Giơnevơ 1954 về Đông Dương, mặc dù sự thật rõ ràng là chính họ đã bán rẻ những lợi ích đó. Từ năm 1965, họ nắm Pôn Pốt, thúc đẩy y cùng đồng bọn tiến hành đấu tranh vũ trang chống chính quyền Xihanúc đang liên minh với các lực lượng kháng chiến Việt Nam và Lào. Nửa cuối năm 1969, sau khi Lon Non lên làm thủ tướng, những người cầm quyền Trung Quốc ủng hộ quan điểm của Lon Non là Quân giải phóng miền Nam Việt Nam phải rút khỏi các căn cứ ở Campuchia và không được dùng cảng Xihanucvin vào việc vận chuyển hậu cần. Chính trong thời gian này, bè lũ Pôn Pốt Iêngxary cũng đòi Quân giải phóng miền Nam Việt Nam rút khỏi các căn cứ ở Campuchia.

Trái với sự mong ước của Bắc Kinh, Mặt trận nhân dân thế giới ủng hộ Việt Nam vẫn hình thành, trên thực tế, tình đoàn kết giữa nhân dân các nước Đông Dương ngày càng củng cố và nhân dân Việt Nam càng đánh càng mạnh, càng giành thêm nhiều thắng lợi.

### 3 -Ngăn cản cuộc thương lượng của Việt Nam với Mỹ để kéo Mỹ xuống thang.

Ngay từ đầu cuộc kháng chiến chống Mỹ xâm lược, chủ trương của Việt Nam là tiến hành đấu tranh trên ba mặt trận quân sự, chính trị và ngoại giao.

Đầu năm 1968, khi cuộc chiến tranh của Mỹ đến đỉnh cao nhất, nhân dân Việt Nam đã tiến hành thắng lợi cuộc tổng tiến công và nổi dậy Tết Mậu Thân, giáng cho địch một đòn quyết liệt, làm đảo lộn thế chiến lược của chúng, buộc chính quyền Mỹ phải xuống thang chiến tranh và ngồi vào đàm phán với Việt Nam dân chủ cộng hoà tại Pari.

Trong cuộc đàm phán với phía Việt Nam tháng 4 năm 1968, phía Trung Quốc thừ nhận rằng Tuyên bố ngày 28 tháng 1 năm 1967 của Chính phủ nước Việt Nam dân chủ cộng hoà về việc đàm phán với Mỹ đã gây ảnh hưởng tốt: ``Ngay đồng minh của Mỹ, cả Đờ Gôn cũng đòi chấm dứt ném bom không điều kiện''. Nhưng họ vẫn cho rằng:

``Lúc này Việt Nam chấp nhận đàm phán chưa phải là thời cơ và trên tư thế cao, ta đã nhân nhượng một cách vội vã''.

Từ ngày 13 tháng 5 năm 1968, ngày khai mạc Hội nghị Pari đến trung tuần tháng 10 năm 1968, bộ máy tuyên truyền của Bắc Kinh không đưa một tin nào về cuộc đàm phán giữa Việt Nam và Mỹ, nhưng lại nhấn mạnh rằng nhân dân Việt Nam cần giải quyết số phận cuộc đấu tranh của mình ``không phải trên bàn hội nghị mà trên chiến trường'', thậm chí còn đe doạ rằng: ``nếu miền Nam Việt Nam không được bảo đảm thì cuối cùng sẽ đưa đến chỗ mất toàn bộ Việt Nam''.

Xu hướng Mỹ buộc phải chấm dứt ném bom miền bắc Việt Nam càng lộ rõ thì phản ứng của Bắc Kinh càng mạnh.

Ngày 9 tháng 10 năm 1968, một nhà lãnh đạo Trung Quốc đã gặp Thứ trưởng Bộ Ngoại thương Việt Nam tại Bắc Kinh và yêu cầu báo cáo với lãnh đạo Việt Nam rằng họ coi việc Mỹ chấm dứt ném bom miền bắc Việt Nam là ``sự thoả hiệp của Việt Nam và Mỹ'', ``là một thất bại lớn, tổn thất lớn đối với nhân dân Việt Nam giống như cuộc đàm phán ký Hiệp nghị Giơnevơ năm 1954 là một sai lầm''; họ đề nghị phía Việt Nam ``nên để cho Mỹ bắn phá trở lại khắp miền bắc, làm như vậy là để Mỹ phân tán mục tiêu oanh tạc, đồng thời cũng chia sẻ bớt khó khăn cho miền Nam''.

Ngăn cản nhân dân Việt Nam thương lượng với Mỹ, khuyến khích Mỹ tăng cường ném bom miền bắc Việt Nam, đây chính là cái mà tướng Mỹ M. Taylơ gọi là quyết tâm của những người lãnh đạo Trung Quốc ``đánh Mỹ đến người Việt Nam cuối cùng'', nhằm làm suy yếu Việt Nam, có lợi cho chính sách bành trướng của họ.

Trong cuộc gặp Thứ trưởng Bộ Ngoại thương Việt Nam nói trên, phía Trung Quốc còn trắng trợn vu khống Việt Nam đàm phán với Mỹ là do ``nghe lời của Liên Xô'' và yêu cầu phía Việt Nam lựa chọn:

``Hoặc là muốn đánh thắng Mỹ thì phải cắt quan hệ với Liên Xô, hoặc là muốn thoả hiệp với Mỹ, dùng viện trợ của Trung Quốc đánh Mỹ để đạt mong muốn đàm phán với Mỹ thì sự viện trợ của Trung Quốc sẽ mất hết ý nghĩa của nó''.

Ngày 17 tháng 10 năm 1968, bộ trưởng Bộ Ngoại giao Trần Nghị gặp đại diện Việt Nam thông báo tuyên bố của những người lãnh đạo Trung Quốc về cuộc đàm phán giữa Việt Nam và Mỹ:

``Lần này nếu các đồng chí chấp nhận bốn bên đàm phán tức là giúp cho Giônxơn và Hămphơrây đoạt được thắng lợi trong bầu cử, để cho nhân dân miền Nam Việt Nam vẫn ở dưới sự đô hộ của đế quốc Mỹ và bù nhìn, không được giải phóng, làm cho nhân dân miền Nam Việt Nam còn có khả năng bị tổn thất lớn hơn…Như vậy giữa hai Đảng và hai nước chúng ta còn cần nói chuyện gì nữa?''.

Đe doạ cắt quan hệ giữa hai Đảng là một thủ đoạn trắng trợn, một sức ép lớn nhất của những người lãnh đạo Trung Quốc đối với Việt Nam.

Đi đôi với việc đe doạ bằng lời lẽ thô bạo là việc đe doạ bằng hành động thực sự. Năm 1968, khi bàn vấn đề viện trợ cho Việt Nam trong năm 1969, những người cầm quyền Bắc Kinh đã giảm kim ngạch viện trợ hơn 20% so với kim ngạch viện trợ năm 1968. Hơn thế nữa, tháng 8 năm 1969 họ trắng trợn nói:

``Thế Việt Nam đánh hay hoà để Trung Quốc tính việc viện trợ?''

Thực tế họ đã giảm kim ngạch viện trợ năm 1970 hơn 50% so với năm 1968.

Sự thật là không phải đến năm 1968 những người lãnh đạo Bắc Kinh mới dùng vấn đề viện trợ để ép Việt Nam. Tháng 4 năm 1966, Tổng bí thư Đảng cộng sản Trung Quốc Đặng Tiểu Bình đã nói với một nhà lãnh đạo Việt Nam rằng năm 1964 ``đồng chí Mao Trạch Đông có phê bình chúng tôi là quá nhiệt tâm đối với vấn đề Việt Nam. Bây giờ chúng tôi mới thấy rõ đồng chí Mao nhìn xa''.

Phía Việt Nam đã trả lời:

``Sự nhiệt tình của một nước xã hội chủ nghĩa đói với một nước xã hội  chủ nghĩa khác là xuất phát từ tinh thần quốc tế vô sản. Chúng tôi không bao giờ nghĩ nhiệt tâm là có hại. Nếu các đồng chí nhiệt tâm giúp đỡ thì chúng tôi có thể đỡ hy sinh 2 hay 3 triệu người…Miền Nam chúng tôi sẽ chống Mỹ đến cùng và chúng tôi vẫn giữ vững tinh thần quốc tế vô sản''

Để tăng sức ép đối với Việt Nam, những người lãnh đạo Trung Quốc còn chỉ thị cho sứ quán của họ ở Hà Nội xúi giục, tổ chức người Hoa ở Việt Nam gây rối, chống lại Chính phủ nước Việt Nam dân chủ cộng hoà. Cùng với hàng vạn người thuộc ``bộ đội hậu cần'' Trung Quốc sang làm đường giúp Việt Nam ở những tỉnh phía bắc từ năm 1965 đến 1968, bọn phản động người Hoa tuyên truyền cho ``tư tưởng Mao Trạch Đông'' và ``cách mạng văn hoá'', xuyên tạc đường lối của Việt Nam, tổ chức các màng lưới gián điệp. Những người cầm quyền Bắc Kinh còn cho những người gọi là ``tị nạn cách mạng văn hoá'' thâm nhập các tỉnh biên giới phía bắc Việt Nam để làm tình báo và tổ chức các ``đội quân ngầm'' (Trong cuộc hội đàm cấp cao Việt - Trung tháng 9 năm 1970, Chủ tịch Mao Trạch Đông đã thừa nhận trách nhiệm của Trung Quốc đối với các hoạt động không hữu nghị đó. Tháng 11 năm 1977. Chủ tịch Hoa Quốc Phong lại một lần nữa, thừa nhận như thế.)

Nhưng nhân dân Việt Nam quyết tâm giữ vững đường lối độc lập tự chủ không gì lay chuyển được của mình. Tất cả những thủ đoạn thô bạo gây sức ép, tất cả những thủ đoạn chính trị của Bắc Kinh đều đã thất bại: Mỹ đã buộc phải chấm dứt không điều kiện việc ném bom miền bắc Việt Nam từ đầu tháng 11 năm 1968 và ngồi vào cuộc Hội nghị bốn bên ở Pari có Mặt trận dân tộc giải phóng miền Nam Việt Nam tham gia từ đầu năm 1969.

## III -THỜI KỲ 1969-1973 : ĐÀM PHÁN VỚI MỸ TRÊN LƯNG NHÂN DÂN VIỆT NAM

Năm 1969, cuộc ``đại cách mạng văn hoá'' ở Trung Quốc về cơ bản hoàn thành. Những người cầm quyền Bắc Kinh bên trong thì ra sức củng cố quyền lãnh đạo của chủ tịch Mao Trạch Đông, ổn định tình hình, đẩy mạnh sản xuất, bên ngoài thì thi hành mọi biện pháp để đẩy nhanh quá trình nhích lại gần đế quốc Mỹ nhằm ngoi lên địa vị một cường quốc lớn, bình thường hoá quan hệ Trung Mỹ và giải quyết vấn đề Đài Loan. Họ mưu toan dùng con bài Việt Nam để đạt mục tiêu đối ngoại đó.

Năm 1969 là năm đầu của Níchxơn vào Nhà Trắng. Ông ta đưa ra cái gọi là ``học thuyết Nichxơn'' nhằm cứu vãn và khôi phục địa vị của đế quốc Mỹ trên thế giới đã bị suy yếu nghiêm trọng do hậu quả của cuộc chiến tranh Việt Nam và bắt đầu thực hiện chiến lược ``Việt Nam   hoá'' chiến tranh nhằm rút được quân Mỹ ra khỏi miền Nam Việt Nam mà vẫn giữ được chính quyền tay sai Nguyễn Văn Thiệu.

Thời kỳ 1969-1973 là thời kỳ nhân dân Việt Nam đẩy mạnh tiến công trên chiến trường cũng như tại Hội nghị bốn bên ở Pari, ngày càng giành thêm nhiều thắng lợi. Đây cũng là thời kỳ Bắc Kinh và Oasinhtơn tăng cường tiếp xúc, bắt tay công khai với nhau, bàn bạc không những các vấn đề tay đôi mà cả các vấn đề thuộc về chủ quyền của nhân dân Việt Nam và của nhân dân các nước ở Đông Dương.

### 1- Công khai phản bội nhân dân Việt Nam

Từ tháng 11 năm 1968, Bộ Ngoại giao Trung Quốc đã ra tuyên bố tỏ ý muốn nối lại các cuộc đàm phán Trung Mỹ ở Vácsava và cùng với Mỹ ký một thoả thuận cùng tồn tại hoà bình. Tiếp đó phía Trung Quốc đã tích cực đáp ứng những tín hiệu của phía Mỹ. Sau khi lên làm tổng thống, Níchxơn báo cho phía Trung Quốc là các cuộc thảo luận giữa Mỹ và Trung Quốc có thể tiến hành ở Trung Quốc. Phía Trung Quốc đã trả lời là `` bản thân Níchxơn có thể đến Bắc Kinh hoặc cử một phái viên đến để thảo luận về vấn đề Đài Loan'' (Ét-ga Xnâu: Cuộc cách mạng lâu dài, Nhà xuất bản Hớt xin xơn, Lân đơn, 1973, tr. 11)

Tháng 6 năm 1970, Trung Quốc và Mỹ thoả thuận là đại sứ Trung Quốc Hoàng Chấn và Kitxinhgiơ sẽ tiến hành những cuộc đàm phán bí mật mỗi lần Kítxinhgiơ đến Pari đàm phán với phía Việt Nam'' (V.A. Ôan-tơ: Những chuyến công du thầm lặng. Nhà xuất bản Đáp-bơn-đê, Niu Oóc, 1978, tr. 530-531)

Ngày 10 tháng 12 năm 1970, qua người bạn thân tín Étga Xnâu, chủ tịch Mao Trạch Đông đưa ra lời mời Tổng thống Níchxơn sang thăm Trung Quốc. :

``Ông ta chắc chắn sẽ được hoan nghênh vì hiện nay những vấn đề giữa Trung Quốc và Mỹ phải được giải quyết với Níchxơn'' (Ét-ga Xnâu: Cuộc cách mạng lâu dài, Nhà xuất bản Hớt xin xơn, Lân đơn, 1973, tr. 172)

Đây là bước ngoặt của Bắc Kinh có ý nghĩa quyết định trong quan hệ Trung Mỹ, đồng thời là bước ngoặt công khai phản bội cách mạng Việt Nam và cách mạng Đông Dương, phản bội cách mạng thế giới. Bắc Kinh tăng cường các cuộc tiếp xúc công khai với Mỹ:

Tháng 3 năm 1971, Trung Quốc mời một đoàn bóng bàn Mỹ sang thăm Trung Quốc, mở đầu cái mà dư luận thế giới gọi là ``ngoại giao bóng bàn''.

Tháng 7 năm 1971 và tháng 10 năm 1971 Kitxinhgiơ, đặc phái viên của Níchxơn sang Bắc Kinh.

Tháng 2 năm 1972, Níchxơn sang thăm Trung Quốc.

Thông báo cho phía Việt Nam biết cuộc đi thăm Bắc Kinh lần thứ nhất của Kítxinhgiơ, ngày 13 tháng 7 năm 1971 Đoàn đại biểu cấp cao Trung Quốc đã nói:

``Vấn đề Đông Dương là quan trọng nhất trong cuộc gặp gỡ giữa chúng tôi và Kítxinhgiơ. Kítxinhgiơ nói rằng Mỹ gắn việc giải quyết vấn đề Đông Dương với việc giải quyết vấn đề Đài Loan. Mỹ nói có rút được quân Mỹ ở Đông Dương thì mới rút quân Mỹ ở Đài Loan. Đối với Trung Quốc, vấn đề rút quân Mỹ khỏi miền Nam Việt Nam là vấn đề số 1. Còn vấn đề Trung Quốc vào Liên hợp quốc là vấn đề số 2''

Khi Níchxơn kết thúc cuộc đi thăm Trung Quốc, hai bên đã ký Thông cáo Thượng Hải ghi nhận kết quả hội đàm giữa hai bên, trong đó có một đoạn như sau:

``Mỹ khẳng định mục tiêu cuối cùng là rút hết các lực lượng và cơ sở quân sự ra khỏi Đài Loan. Trong khi chờ đợi, tuỳ theo tình hình căng thẳng trong khu vực này giảm đi, Mỹ sẽ dần dần giảm lực lượng và cơ sở quân sự của Mỹ ở Đài Loan''

Đầu tháng 3 năm 1972, khi thông báo cho phía Việt Nam về cuộc hội đàm vơi Níchxơn, đại diện những người lãnh đạo Trung Quốc đã giải thích về đoạn thông cáo trên như sau:
``Muốn bình thường hoá quan hệ Trung Mỹ, muốn làm dịu tình hình ở Viễn Đông thì trước hết phải giải quyết vấn đề Việt Nam và Đông Dương. Chúng tôi không đòi giải quyết vấn đề Đài Loan trước. Vấn đề Đài Loan là bước sau''

Thâm tâm của Bắc Kinh là lợi dụng vấn đề Việt Nam để  giải quyết trước vấn đề Đài Loan. Nhưng Việt Nam kiên quyết giữ vững đường lối độc lập tự chủ của mình. Do đó những người lãnh đạo Trung Quốc và Tổng thống Nichxơn mới thoả thuận: ``Trong khi chờ đợi, tuỳ theo tình hình căng thẳng trong khu vực này giảm đi…''

Điều đó có nghĩa là nếu Bắc Kinh muốn thúc đẩy việc rút lực lượng và các cơ sở quân sự của Mỹ ra khỏi Đài Loan thì họ cần ép Hà Nội đi vào một giải pháp thoả hiệp với Mỹ.

Phía Trung Quốc dùng ``củ cà rốt'' viện trợ: nếu năm 1968 vì phản đối Việt Nam đàm phán với Mỹ họ đã giảm kim ngạch viện trợ cho Việt Nam thì năm 1971 và năm 1972, để lôi kéo Việt Nam đi vào chiều hướng của Bắc Kinh thoả hiệp với Mỹ, họ đã dành cho Việt Nam viện trợ cao nhất của họ so với những năm trước đó. Đây cũng là thủ đoạn nhằm che đậy sự phản bội của họ nhằm xoa dịu sự công phẩn của nhân dân Việt Nam.

Đi đôi với tăng thêm viện trợ là sự thúc ép liên tục để Việt Nam chấp nhận giải pháp của Mỹ. Ngày 18 tháng 7 năm 1971, phía Trung Quốc thông báo cho phía Việt Nam phương án bốn điểm của Mỹ: rút quân và thả tù binh Mỹ trong 12 tháng kể từ ngày 1 tháng 8 năm 1971, ngừng bắn toàn Đông Dương và giải pháp theo kiểu Giơnevơ năm 1954. Về rút quân, ``vì thể diện'' Mỹ muốn để lại một số cố vấn kỹ thuật; về chính trị ``Mỹ không muốn bỏ Nguyễn Văn Thiệu cũng như không muốn bỏ Xirích Matắc''

Trong cuộc hội đàm với phía Việt Nam tháng 11 năm 1971 họ nói:

``Việt Nam nên tranh thủ thời cơ giải quyết trước vấn đề rút quân Mỹ và quan tâm giải quyết vấn đề tù binh Mỹ, việc đánh đổ nguỵ quyền Sài Gòn là lâu dài''

Cũng trong dịp này, sau khi nhắc lại ý của phía Mỹ là ``Mỹ có nhiều bạn cũ, Mỹ phải giữ'' chủ tiach Mao Trạch Đông nói:

``Vấn đề Đài Loan là vấn đề trường kỳ. Có lẽ mấy năm không giải quyết xong. Nếu xét nhanh hay chậm thì tôi thiên về chậm hơn. Hiện nay Tưởng có 65 vạn quân, ở giữa lại có eo biển. Chúng tôi không sang được, nó vẫn cứ đóng ở đó, chổi không đến nơi thì bụi không đi''

Sau khi Níchxơn kết thúc chuyến đi thăm Trung Quốc, Kítxinhgiơ nói với các nhà báo ngày 1 tháng 3 năm 1972 rằng từ nay Níchxơn và bản thân y ``chỉ còn việc nhìn về Mátxcơva và nghiền nát Việt Nam'' (Me-vin và Béc -na Can-bê: Kít-xinh-giơ. Nhà xuất bản Lít-tơn Brao, Tô-rông-tô, 1974, tr.283)

Từ tháng 4 năm 1972, Mỹ ném bom lại và thả mìn phong toả các cảng miền bắc Việt Nam và đánh phá ác liệt miền Nam Việt Nam nhằm đối phó với cuộc tiến công chiến lược mùa xuân năm 1972 của nhân dân Việt Nam, cứu vãn sự sụp đổ của chế độ Nguyễn Văn Thiệu.
Bước phiêu lưu quân sự này chính là hậu quả rõ ràng của sự đồng loã giữa những người cầm quyền Trung Quốc và Níchxơn.

Việc Hiệp định Pari không được ký tắt vào cuối tháng 10 năm 1972, ai cũng rõ đó là do sự lật lọng của Níchxơn- Kítxinhgiơ. Nhưng những người cầm quyền Trung Quốc lại đứng trên quan điểm của Mỹ để gây sức ép với Việt Nam. Ngày 1 tháng 11 năm 1972, họ yêu cầu Thứ trưởng Bộ Ngoại thương Việt Nam dân chủ cộng hoà báo   cáo với lãnh đạo Việt Nam: Việt Nam nên nhân nhượng về hai vấn đề rút quân miền bắc và miền bắc Việt Nam không nhận viện trợ quân sự để có thể ký kết được hiệp định.

Và ngày 5 tháng 12 năm 1972, đại sứ Trung Quốc Hoàng Chấn chuyển tới phía Việt Nam lời đe doạ của Kitxinhgiơ:

``Đàm phán đã đến lúc có hậu quả nghiêm trọng: Bắc Việt Nam đòi Mỹ hoặc trở lại hiệp định cũ, hoặc nhận một hiệp định xấu hơn: Mỹ không thể chấp nhận cả hai điều kiện đó. Nếu Việt Nam cứ giữ lập trường đó thì đàm phán đứt quãng và Mỹ sẽ có mọi hành động bảo vệ nguyên tắc của mình''

Đó chính là giọng lưỡi chuẩn bị cho cuộc tập kích chiến lược bằng máy bay B52 những ngày cuối năm 1972 nhằm huỷ diệt Hà Nội và Hải Phòng, hòng khuất phục nhân dân Việt Nam, buộc nhân dân Việt Nam chấp nhận giải pháp do đế quốc Mỹ áp đặt.

Trước sự câu kết của Bắc Kinh với Oasinhtơn phản bội nhân dân Việt Nam, nhân dân Việt Nam vẫn kiên cường đẩy mạnh cuộc kháng chiến chống Mỹ và tin tưởng vào thắng lợi của mình.

Khi phía Trung Quốc thông báo với phía Việt Nam rằng trong chuyến thăm Trung Quốc, Níchxơn cũng sẽ cùng những người lãnh đạo Trung Quốc bàn về vấn đề Việt Nam, những người lãnh đạo Việt Nam đã thẳng thắn nói:

``Việt Nam là của chúng tôi; các đồng chí không được bàn với Mỹ về vấn đề Việt Nam. Các đồng chí đã nhận sai lầm năm 1954 rồi, bây giờ không nên sai lầm một lần nữa''

Khi phía Trung Quốc thông báo chuyến đi thăm Trung Quốc của Níchxơn, những người lãnh đạo Việt Nam nói:

``Trong cuộc chiến đấu chống Mỹ, nhân dân Việt Nam phải thắng. Tới đây, đế quốc Mỹ có thể đánh phá trở lại miền bắc ác liệt hơn nữa, nhưng nhân dân Việt Nam không sợ, nhân dân Việt Nam nhất định thắng''

Bất chấp mọi sức ép của Bắc Kinh và Oasinhtơn, nhân dân Việt Nam không những không nhân nhượng về những vấn đề có tính nguyên tắc, mà còn trừng trị đích đáng đế quốc Mỹ về những tội ác của chúng và cuối cùng buộc Mỹ phải ký Hiệp định Pari về Việt Nam ngày 27 tháng 1 năm 1973.

### 2 -Nắm trọn vấn đề Campuchia

Trong khuôn khổ đường lối hoà hoãn và câu kết với đế quốc Mỹ, dọn con đường bành trướng xuống Đông Nam châu Á sau này, đồng thời phá hoại Mặt trận đoàn kết nhân dân các nước ở Đông Dương, gây thêm sức ép đối với Việt Nam, từ năm 1970 Bắc Kinh tìm cách nắm các lực lượng Campuchia, thi hành một chính sách rất phức tạp đối với Campuchia, nhưng trước sau chỉ nhằm một mục tiêu: lợi ích ích kỷ của họ.

Dư luận còn nhớ bọn đế quốc và phản động đã gây ra cuộc đảo chính ngày 18 tháng 3 năm 1970, lật đổ chính phủ của ông hoàng Nôrôđôm Xihanúc, đưa lon Non lên cầm quyền. Lon Non vốn là người Campuchia gốc Hoa, lại là người của Mỹ, cho nên những người lãnh đạo Trung Quốc muốn dùng y và bỏ rơi ông Xihanúc. Tại Bắc Kinh, Bộ Ngoại giao Trung quốc nói với đại sứ Việt Nam:

``Xihanúc không có lực lượng. Việt Nam cần ủng hộ Lon Non; Trung Quốc đón Xihanúc (1) nhưng vẫn quan hệ tốt với đại sứ quán của PhnômPênh ``

(1) Khi đó ông Xi-ha-núc vừa kết thúc chuyến đi thăm Liên-Xô và đang trên đường đi Bắc kinh.

Tại PhnômPênh, đại sứ Trung Quốc cũng nói những điều tương tự với đại sứ Việt Nam. Bộ Ngoại giao Trung Quốc còn nói với đại sứ Việt Nam ở Bắc Kinh là Trung Quốc không đồng ý để sinh viên Việt Nam ở Trung Quốc biểu tình chống Lon Non.

Ngay sau khi nổ ra cuộc đảo chính ở PhnômPênh và ông Xihanúc tới Bắc Kinh, thủ tướng Phạm Văn Đồng đã sang Trung Quốc thuyết phục những người lãnh đạo Trung Quốc nên ủng hộ ông Xihanúc, đồng thời trực tiếp biểu thị với ông Xihanúc sự ủng hộ mạnh mẽ của Việt Nam đối với ông ta và lực lượng kháng chiến Khơme.

Ngày 23 tháng 3 năm 1970, ông Xihanúc công bố bản tuyên cáo 5 điểm lên án cuộc đảo chính của Lon Non và kêu gọi nhân dân Campuchia đoàn kết chống đế quốc Mỹ và bè lũ Lon Non.

Ngày 25 tháng 3 năm 1970, Chính phủ Việt Nam dân chủ cộng hoà tuyên bố hoàn toàn ủng hộ bản Tuyên cáo đó.

Ngày 7 tháng 4 năm 1970, Chính phủ Trung Quốc mới ra tuyên bố ủng hộ Tuyên cáo của ông Xihanúc. Tuy vậy, họ tiếp tục có những cuộc tiếp xúc bí mật với chính quyền Lon Non. Trong khi đó, với sự giúp đỡ của các lực lượng vũ trang Việt Nam, lực lượng kháng chiến Khơme giành thêm nhiều chiến thắng mới, giải phóng một phần tư đất nước. Chỉ sau khi Níchxơn đưa quân Mỹ xâm lược Campuchia, gây nên một làn sóng phản đối mạnh mẽ trên thế giới và ngay cả ở Mỹ, Chính phủ Trung Quốc mới cắt đứt quan hệ với chính quyền Lon Non ngày 5 tháng 5 năm 1970.

Rõ ràng là do Việt Nam kiên quyết ủng hộ Chính phủ kháng chiến Campuchia và do tình hình thực tế trên chiến trường phát triển có lợi cho các lực lượng kháng chiến, những người cầm quyền Bắc Kinh mới chuyển sang ủng hộ ông Xihanúc, Chủ tịch Mặt trận thống nhất dân tộc Campuchia chống Mỹ xâm lược. Từ đó họ tìm cách nắm chặt ông Xihanúc làm con bài độc quyền của họ để chuẩn bị cho những cuộc mặc cả với Mỹ. Tuy ủng hộ ông Xihanúc và Chính phủ kháng chiến Campuchia, họ vẫn ngấm ngầm duy trì những quan hệ bí mật với bè lũ Lon Non-Xirích Matắc, mặt khác tích cực dùng bọn Pôn Pốt-IêngXary, dần dần biến Đảng Khơme thành một đảng phụ thuộc vào Đảng cộng sản Trung Quốc như kiểu các đảng, các nhóm theo Mao ở Đông Nam châu Á và ở một số nước khác trên thế giới.

Những người lãnh đạo Trung Quốc đã lợi dụng Hội nghị cấp cao ba nước Đông Dương lần thứ nhất tháng 4 năm 1970 và cố tình làm cho dư luận thấy rằng họ đã ``đóng góp'' nhiều vào hội nghị đó. Họ muốn chứng tỏ cho Mỹ hiểu rằng họ có thể giúp Mỹ tìm một giải pháp cho cả vấn đề Đông Dương và chính họ là người ``thay mặt'' cho Việt Nam và các nước khác ở Đông Dương để đàm phán với Mỹ.

Trong bối cảnh nhân dân Việt Nam, nhân dân Lào và nhân dân Campuchia liên tiếp giáng những đòn mạnh mẽ vào các kế hoạch phiêu lưu quân sự của Níchxơn, ông Xihanúc đề nghị triệu tập Hội nghị cấp cao lần thứ hai của nhân dân các nước Đông Dương vào cuối năm 1971, nhằm phát huy thắng lợi, đẩy mạnh cuộc chiến đấu chống Mỹ của nhân dân các nước ở Đông Dương. Bề ngoài những người lãnh đạo Trung Quốc tán thành đề nghị đó, nhưng bên trong họ giật dây bọn Pôn Pốt-Iêng Xary phản đối. Mặt khác, nhân chuyến đi thăm Việt Nam tháng 3 năm 1971, họ gợi ý triệu tập hội nghị 5 nước 6 bên (hai miền Nam, Bắc Việt Nam, Lào, Campuchia, Trung Quốc và Cộng hoà dân chủ nhân dân Triều Tiên) trên đất Trung Quốc nhằm mục tiêu chống Nhật. Ý đồ của họ là phá hoại khối đoàn kết, lái chệch mục tiêu đấu tranh của nhân dân các nước ở Đông Dương, đồng thời tập hợp lực lượng dưới sự chỉ đạo của  Bắc Kinh để họ có thêm thế đi vào đàm phán với Mỹ. Phía Việt Nam ủng hộ đề nghị của ông Xihanúc, không tán thành họp hội nghị 5 nước 6 bên như Trung Quốc gợi ý, cũng không tán thành quan điểm cho rằng nguy cơ bấy giờ là Nhật vì kẻ thù chính của nhân dân ba nước Đông Dương vẫn là đế quốc Mỹ xâm lược. Do đó, kế hoạch hội nghị 5 nước 6 bên của Bắc Kinh đã hoàn toàn thất bại.

Trong lúc tìm cách nắm trọn vấn đề Campuchia, những người lãnh đạo Trung Quốc còn mưu toan nắm con đường vận chuyển quân sự qua ba nước Đông Dương. Trong mấy năm liền cho đến năm 1972, họ đề nghị giúp làm đường và vận chuyển hàng phục vụ chiến trường từ miền bắc đến miền Nam Việt Nam, Lào và Campuchia trên con đường mòn Hồ Chí Minh và hứa cung cấp cho Việt Nam đủ xe, người lái và khoảng 20 vạn quân nhân Trung Quốc để bảo đảm công việc này. Ý đồ của họ là qua đó nắm toàn bộ vấn đề Đông Dương để buôn bán với Mỹ và chuẩn bị bàn đạp đi xuống Đông Nam châu Á. Tất nhiên phía Việt Nam không chấp nhận đề nghị đó.

◎◎◎

Nếu trước đây những người lãnh đạo Trung Quốc ngấm ngầm làm yếu cuộc kháng chiến của nhân dân Việt Nam thì trong thời kỳ 1969-1973, nhất là từ năm 1971, họ công khai câu kết với đế quốc Mỹ xâm lược, cứu nguy cho chúng trước cuộc tiến công chiến lược mới của nhân dân Việt Nam, lấy con bài Việt Nam để buôn bán với Mỹ. Nếu trước đây họ ngấm ngầm chia rẽ nhân dân ba nước Việt Nam, Lào và Campuchia nhằm cô lập Việt Nam thì trong thời kỳ này họ bắt đầu dùng bè lũ Pôn Pốt-Iêng Xary để phá hoại cách mạng ba nước Đông Dương, tích cực chuẩn bị biến Campuchia thành bàn đạp để tiến công Việt Nam, khống chế bán đảo Đông Dương, từ đó bành trướng xuống Đông Nam châu Á sau này.

Họ đã phơi trần bộ mặt ghê tởm của kẻ phản bội: phản bội nhân dân Việt Nam cũng như phản bội nhân dân Lào và nhân dân Campuchia.

## IV- THỜI KỲ 1973-1975: CẢN TRỞ NHÂN DÂN VIỆT NAM GIẢI PHÓNG HOÀN TOÀN MIỀN NAM

Theo hiệp định Pari về Việt Nam, Mỹ phải rút hết đội quân viễn chinh của Mỹ ra khỏi miền Nam Việt Nam, cam kết tôn trọng các quyền dân tộc cơ bản của nhân dân Việt Nam, thừa nhận ở miền Nam Việt Nam có hai vùng, hai chính quyền, hai quân đội và ba lực lượng chính trị, và các bên Việt Nam cùng thành lập một Chính phủ liên hiệp ba thành phần ở miền Nam Việt Nam. Đó là thắng lợi của đường lối độc lập, tự chủ và cuộc chiến đấu ngoan cường của nhân dân Việt Nam, là thắng lợi của phong trào nhân dân thế giới đoàn kết với nhân dân Việt Nam. Đó là thất bại của sự mua bán bẩn thỉu trên lưng nhân dân Việt Nam giữa chính quyền Níchxơn và những người lãnh đạo Trung Quốc thể hiện trong Thông cáo Thượng Hải.

Chính phủ Việt Nam dân chủ cộng hoà và Chính phủ cách mạng lâm thời Cộng hoà miền Nam Việt Nam luôn luôn chủ trương thi hành nghiêmm chỉnh Hiệp định Pari và đòi Mỹ Thiệu cũng phải có thái độ như vậy.

Nhưng Mỹ Thiệu mưu toan phá hoại việc thi hành Hiệp định đó, chỉ muốn thi hành những điều khoản có lợi cho Mỹ. Đối với các điều khoản khác họ vi phạm ngay từ phút đầu. Lúc Hiệp định Pari bắt đầu có hiệu lực cũng là lúc Mỹ Thiệu đưa hàng vạn quân có máy bay, trọng pháo và xe tăng yểm trợ đổ bộ lên Cửa Việt hòng chiếm lấy vùng giải phóng phía bắc tỉnh Quảng Trị. Sau đó quân nguỵ liên tiếp tấn công trên khắp miền Nam Việt Nam, lấn chiếm nhiều vùng giải phóng do Chính phủ cách mạng lâm thời Cộng hoà miền Nam Việt Nam quản lý. Âm mưu của Mỹ nguỵ là xoá bỏ tình hình thực tế có hai vùng, hai chính quyền, đặt lại toàn bộ miền Nam Việt Nam dưới ách thống trị thực dân kiểu mới, kéo dài chia cắt nước Việt Nam.

Những người lãnh đạo Trung Quốc tỏ vẻ hoan nghênh Hiệp định Pari về Việt Nam. Trên thực tế để thực hiện sự thoả thuận với Mỹ và tăng cường câu kết với Mỹ, đồng thời tiếp tục làm suy yếu hòng khuất phục Việt Nam, họ tìm mọi cách cản trở cuộc đấu tranh của nhân dân Việt Nam nhằm đánh bại âm mưu của Mỹ Thiệu phá hoại Hiệp định Pari, giải phóng hoàn toàn miền Nam và thống nhất nước nhà.

### 1- Kiềm chế cuộc chiến đấu của nhân dân Việt Nam chống Mỹ Thiệu phá hoại Hiệp định Paris.

Trong cuộc hội đàm ở Bắc Kinh tháng 6 năm 1973, chủ tịch Mao Trạch Đông nói với Tổng bí thư Lê Duẩn và Thủ tướng Phạm Văn Đồng:

``Ở miền Nam Việt Nam cần ngừng (chiến đấu) nửa năm, một năm, một năm rưỡi, hai năm càng tốt''. Cách mạng miền Nam nên ``chia làm hai bước . Gộp lại làm một, người Mỹ không chịu đâu. Vấn đề là trong tay chính quyền Nguyễn Văn Thiệu còn có mấy chục vạn quân''.

Và ông ta nhắc lại đến luận điểm ``cái chổi'' đã nói với phía Việt Nam trước đây.

Thủ tướng Chu Ân Lai thì nói:

``Trong một thời gian chưa có thể nói dứt khoát là 5 năm hay 10 năm, Việt Nam và Đông Dương nghỉ ngơi được thì càng tốt; tranh thủ thời gian đó mà nhân dân miền Nam Việt Nam, Lào, Campuchia thực hiện hoà bình, trung lập một thời gian''.

Để tỏ thiện chí với nhân dân Việt Nam, những người lãnh đạo Trung Quốc hứa hẹn sẽ tiếp tục viện trợ trong 5 năm với mức kim ngạch như năm 1973. Sự thật là khi đó họ đã ngừng hoàn toàn viện trợ về quân sự, còn về viện trợ kinh tế họ nhận chủ yếu phục hồi các cơ sở do Trung Quốc giúp trước đây và đã bị Mỹ đánh phá nhưng họ kéo dài việc thực hiện, có nơi không thực hiện.

Những người cầm quyền Trung Quốc thực chất muốn Việt Nam không làm gì cả, kể cả khi chính quyền Sài Gòn đưa quân lấn chiếm vùng giải phóng.

Đứng trước những hành động của quân nguỵ Sài Gòn lấn chiếm vùng giải phóng ngày càng tăng cường, tháng 10 năm 1973, Bộ chỉ huy các lực lượng vũ trang miền Nam Việt Nam buộc phải ra lệnh kiên quyết đánh trả. Gần một tháng sau đó, trong chuyến Kítxinhgiơ đi thăm Bắc Kinh, hai bên ra thông cáo thoả thuận là trong tình hình ``đặc biệt nghiêm trọng hiện nay'', hai bên cần tiến hành liên hệ thường xuyên ở các cấp có thẩm quyền để trao đổi ý kiến về những vấn đề cùng quan tâm. Thực tế đó là sự phối hợp giữa những người cầm quyền Trung Quốc và Mỹ nhằm ngăn cản cuộc đấu tranh của các lực lượng vũ trang miền Nam Việt Nam.

Những người cầm quyền Bắc Kinh còn khuyên Mỹ ``đừng thua ở Việt Nam, đừng rút lui khỏi Đông Nam châu Á''. (Theo tướng A. Hai-gơ, báo Mỹ Người hướng dẫn khoa học cơ đốc, ngày 20 tháng 6 năm 1979.)

Thâm độc hơn nữa, họ tìm cách lôi kéo nhiều tướng tá và quan chức nguỵ quyền Sài Gòn hợp tác với họ, thậm chí cho người thuyết phục tướng Dương Văn Minh, ``tổng thống'' vào những ngày cuối của chế độ Sài Gòn để tiếp tục chống lại cuộc tổng tiến công và nổi dậy của quân và đan miền Nam Việt Nam.

### 2 -Xâm chiếm lãnh thổ của Việt Nam, gây căng thẳng ở biên giới

Từ năm 1973, những người cầm quyền Trung Quốc tăng cường những hành động khiêu khích và lấn chiếm đất đai ở những tỉnh biên giới phía bắc Việt Nam, nhằm làm yếu những cố gắng của nhân dân Việt Nam trong cuộc đấu trang giải phóng hoàn toàn miền Nam.

Đồng thời họ ngăn cản Việt Nam thăm dò, khai thác tài nguyên thiên nhiên của mình để nhanh chóng khôi phục và phát triển kinh tế. Ngày 26 tháng 12 năm 1973, phía Việt Nam đề nghị mở cuộc đàm phán để xác định chính thức đường biên giới giữa Việt Nam và Trung Quốc trong vịnh Bắc Bộ, nhằm sử dụng phần biển thuộc Việt Nam phục vụ công cuộc xây dựng đất nước. Ngày 18 tháng 1 năm 1974 phía Trung Quốc trả lời chấp thuận đề nghị trên, nhưng họ đòi không được tiến hành việc thăm dò trong một khu vực rộng 20.000 km2 trong vịnh Bắc Bộ do họ tự ý định ra. Họ còn đòi ``không để một nước thứ ba vào thăm dò vịnh Bắc Bộ'', vì việc đưa nước thứ ba vào thăm dò ``không có lợi cho sự phát triển kinh tế chung của hai nước và an ninh quân sự của hai nước''. Đó chỉ là một lý do để che đậy ý đồ đen tối của họ. Cũng vì vậy cuộc đàm phán về đường biên giới giữa Việt Nam và Trung Quốc trong vịnh Bắc Bộ từ tháng 8 đến tháng 11 năm 1974 đã không đi đến kết quả tích cực nào. Cũng với thái độ trịch thượng nước lớn như vậy, họ làm bế tắc cuộc đàm phán về vấn đề biên giới trên bộ và trong vịnh Bắc Bộ bắt đầu từ tháng 10 năm 1977 nhằm mục đích tiếp tục xâm phạm biên giới, lấn chiếm lãnh thổ Việt Nam để duy trì tình hình căng thẳng ở biên giới Việt Trung.

Hơn nữa, ngày 19 tháng 1 năm 1974, tức là một ngày sau khi phía Trung Quốc nhận đàm phán với phía Việt Nam về vấn đề vịnh Bắc Bộ, họ sử dụng lực lượng hải quân và không quân tiến đánh quân nguỵ Sài Gòn và chiếm đóng quần đảo Hoàng Sa từ lâu vốn là bộ phận lãnh thổ Việt Nam. Họ nói là để tự vệ, nhưng thực chất đó là một hành động xâm lược, một sự xâm chiếm lãnh thổ Việt Nam để khống chế Việt Nam từ mặt biển và từng bước thực hiện mưu đồ độc chiếm biển Đông. Hành động xâm lược của họ có tính toán từ trước và được sự đồng tình của Mỹ. Vì vậy, khi đó đại sứ Mỹ G. Matin ở Sài Gòn đã bác bỏ yêu cầu cứu viện của chính quyền Nguyễn Văn Thiệu và hạm đội Mỹ ở Thái Bình Dương đã được lệnh tránh xa quần đảo Hoàng Sa.

Trong cuộc hội đàm với những người lãnh đạo Việt Nam năm 1975, phó thủ tướng Đặng Tiểu Bình đã thừa nhận rằng hai bên đều nói các quần đảo Hoàng Sa và Trường Sa là của mình, cho nên cần gặp gỡ để bàn bạc giải quyết. Điều đó càng chứng tỏ hành động của phía Trung Quốc xâm chiếm quần đảo Hoàng Sa là ngang ngược, bất chấp luật pháp quốc tế, gây ra một tình trạng việc đã rồi. (Về vấn đề biên giới, các quần đảo Hoàng Sa và Trường Sa và các cộc đàm phán về vịnh Bắc Bộ và biên giới, xin tham khảo bị vong lục của Bộ Ngoại Giao nước Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam ngày 7 tháng 8 năm 1979 và ngày 27 tháng 9 năm 1979, và Sách trắng của Bộ Ngoại Giao nước Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam công bố ngày 28 tháng 9 năm 1979)

### 3- Biến Campuchia thành bàn đạp chuẩn bị tiến công Việt Nam

Sau Hiệp định Pari về Việt Nam, theo lệnh của Bắc Kinh, bè lũ Pôn Pốt-Iêng Xary thi hành một chính sách hai mặt đối với Việt Nam: vừa dựa vào Việt Nam vừa chống Việt Nam.
Chúng tỏ ra ``hữu nghị'' và ``đoàn kết'' với Việt Nam để tranh thủ sự giúp đỡ của Việt Nam, đặc biệt là khi chuẩn bị tấn công vào thủ đô Phnôm Pênh. Trong khuôn khổ sự thoả hiệp Trung Mỹ, những người cầm quyền Trung Quốc thực hiện sự thoả thuận không viện trợ quân sự cho cách mạng ba nước Đông Dương. Họ đã từ chối đề nghị của bè lũ Pôn Pốt Iêng Xary về tăng viện vũ khí tiến công bằng cách nhờ Việt Nam cho Pôn Pốt Iêng Xary vay rồi Trung Quốc sẽ thanh toán sau. Đây là một thủ đoạn của Bắc Kinh vừa lấy lòng bọn tay sai ở Campuchia, tránh va chạm với Mỹ, vừa gây thêm khó khăn cho Việt Nam trong lúc Việt Nam đang tiến hành cuộc tổng tiến công và nổi dậy mùa xuân năm 1975.

Mặt khác, bè lũ Pôn Pốt Iêng Xary tìm mọi cách chống Việt Nam. Chúng vu cáo Việt Nam ký kết Hiệp định Pari là ``phản bội Campuchia một lần nữa'' để kích động hận thù dân tộc, gây tâm lý chống Việt Nam và kiếm cớ thanh trừng những người Campuchia không tán thành đường lối của chúng. Nhiều lần chúng tổ chức những vụ đánh phá, cướp bóc kho tàng, vũ khí, tiến công bệnh viện và nơi trú quân của Quân giải phóng miền Nam Việt Nam đặt trên đất Campuchia.

Bè lũ Pôn Pốt Iêng Xary, bằng những thủ đoạn rất thâm độc, kể cả việc thủ tiêu bí mật những cán bộ cách mạng chân chính, ra sức củng cố vị trí trong Đảng, nắm toàn bộ quyền lực để biến Đảng cộng sản Campuchia thành một đảng phụ thuộc Bắc Kinh.

Rõ ràng những người lãnh đạo Trung Quốc đã tiến thêm một bước trong âm mưu nắm trọn Campuchia dưới chế độ của bọn Pôn Pốt Iêng Xary, chuẩn bị bàn đạp tiến công Việt Nam từ phía tây nam sau khi miền Nam Việt Nam hoàn toàn giải phóng.

Mặc dù những người lãnh đạo Trung Quốc ra sức cản trở nhân dân Việt Nam giải phóng hoàn toàn miền Nam Việt Nam, nhân dân Việt Nam vẫn kiên quyết đấu tranh trên ba mặt trận quân sự, chính trị và ngoại giao chống Mỹ Thiệu phá hoại Hiệp định Pari, tiến lên giành toàn thắng. Với cuộc tổng tiến công và nổi dậy mùa xuân năm 1975, nhân dân Việt Nam đã đánh sụp hoàn toàn chính quyền Nguyễn Văn Thiệu, giải phóng hoàn toàn miền Nam Việt Nam, thống nhất nước nhà.



◎◎◎

Trong cuộc kháng chiến chống thực dân Pháp xâm lược trước đây, những người cầm quyền Bắc Kinh chỉ can thiệp vào giai đoạn chót nhằm áp đặt giải pháp của họ đối với nhân dân Việt Nam cũng như nhân dân Lào và nhân dân Campuchia.

Trong cuộc kháng chiến chống Mỹ cứu nước lần này của nhân dân Việt Nam, họ can thiệp ngay từ đầu, tạo điều kiện cho đế quốc Mỹ tăng cường chiến tranh trên cả nước Việt Nam, ném bom cực kỳ man rợ miền bắc, lấy vấn đề Việt Nam để buôn bán với Mỹ, nhưng luôn luôn làm ra vẻ ``triệt để cách mạng'', ``tích cực'' ủng hộ Việt Nam.

Đây là sự phản bội thứ hai của những người lãnh đạo Trung Quốc đối với nhân dân Việt Nam.

# PHẦN THỨ TƯ

TRUNG QUỐC VỚI NƯỚC VIỆT NAM HOÀN TOÀN GIẢI PHÓNG VÀ THỐNG NHẤT 
===

(TỪ THÁNG 5 NĂM 1975 CHO ĐẾN NAY)

## I.	TRUNG QUỐC SAU THẤT BẠI CỦA MỸ Ở VIỆT NAM

Dư luận chung trên thế giới cho thấy rằng thất bại của Mỹ ở Việt Nam đã có tác động rõ rệt đến tình hình quốc tế.

Nếu thắng lợi của cuộc Cách Mạng Tháng Tám và kháng chiến của nhân dân Việt Nam mở đầu sự sụp đổ của chủ nghĩa thực dân cũ, thì thắng lợi của cuộc kháng chiến chống Mỹ, cứu nước của nhân dân Việt Nam chứng minh trước toàn thể thế giới sự phá sản hoàn toàn của chủ nghĩa thực dân mới là không tránh khỏi.

Ngày nay, các lực lượng cách mạng đã lớn mạnh và có lợi thế hơn bao giờ hết. Đế quốc Mỹ không thể đóng vai sen đầm quốc tế bất cứ nơi đâu mà không bị trừng phạt, không thể xâm chiếm một tấc đất của bất kỳ nước xã hội chủ nghĩa nào, không thể đẩy lùi phong trào giải phóng dân tộc và ngăn chặn con đường phát triển của các nước đi lên chủ nghĩa xã hội. Chủ nghĩa đế quốc càng ngày càng lún sâu vào cuộc khủng hoảng toàn diện không phương cứu chữa. Chúng đang đứng trước những khó khăn chồng chất về nhiều mặt, đang phải đương đầu với cuộc tiến công rộng lớn và mạnh mẽ của ba dòng thác cách mạng trên thế giới, ở ngay cả ở dinh lũy tưởng chừng như rất kiên cố của chúng ở Châu Á và châu Mỹ la tinh.

Mặc dầu đã tung vào Việt Nam một đội quân viễn chinh gồm 60 vạn tên làm nòng cốt cho hơn một triệu quân ngụy, đã ném xuống Việt Nam 7 triệu 85 vạn tấn bom và chi tiêu 352 tỷ dô la, đế quốc Mỹ xâm lược vẫn không khuất phục được nhân dân Việt Nam, đế quốc Mỹ phải điều chỉnh chiến lược trên thế giới, ở châu Á nhất là Đông Nam Á, cho phù hợp với tình hình mới. Chúng đẩy mạnh các cấu kết với các thế lực phản động, đặc biệt là các thế lực phản bội trong phong trào cọng sản và công nhân quốc tế, chủ yếu là với tập đoàn phản động Bắc Kinh, hòng chia rẽ, phá hoại hệ thống xã hội chủ nghĩa và phong trào cách mạng thế giới.

[anh5]
[anh6]

Về phần những người cầm quyền Trung Quốc, xuất phát từ lợ ích dân tộc, họ có giúp Việt Nam khi nhân dân Việt Nam chiến đấu chống Mỹ, nhưng cũng xuất phát từ lợi ích dân tộc, họ không muốn Việt Nam thắng Mỹ và trở nên mạnh, mà chỉ muốn Việt Nam yếu, lệ thuộc Trung Quốc.

Họ muốn Việt Nam bị chia cắt lâu dài, nhưng nhân dân Việt Nam đã đánh cho ``Mỹ cút, ngụy nhào'', giải phóng hoàn toàn miền nam và thống nhất nước nhà.

Họ lợi dụng xương máu của nhân dân Việt Nam để buôn bán với Mỹ, nhưng sự câu kết của họ với Mỹ không ngăn được nhân dân Việt Nam giành thắng lợi hoàn toàn và dựng nên nước Cọng Hòa Xã Hội Chủ Nghĩa Việt Nam.

Họ muốn chia rẽ Việt Nam với Liên Xô và các nước xã hội chủ nghĩa khác, nhưng nhân dân Việt nam đã giữ vững đường lối độc lập, tự chủ của mình, tăng cường đoàn kết với Liên Xô và các nước xã hội chủ nghĩa anh em khác.

Không những các thỏa thuận giữ họ với chính quyền Nixon, những mưu đồ chiến lược của họ đã bị thất bại, mà một nước Việt nam độc lập, thống nhất và xã hội chủ nghĩa, có đường lối cách mạng Max – Lenin chân chính, độc lập, tự chủ và có uy tín chính trị trên thế giới sẽ là cản trở nghiêm trọng cho mưu đồ bành trướng và bá quyền của họ ở Đông dương và Đông nam châu Á. Thắng lợi lịch sử của nhân dân Việt nam không chỉ là thất bại lớn của đế quốc Mỹ xâm lược, mà cũng là thất bại lớn của bọn bành trướng Bắc kinh.

Vào những năm cuối đời và cái chết của Chủ tịch Mao Trạch Đông, cuộc khủng hoảng của Trung quốc tiếp tục diễn ra gay gắt, với những cuộc thanh trừng tàn bạo để tranh giành quyền bính. Trong 20 năm liền, nền kinh tế và tình hình chính trị, xã hội của Trung quốc đã bị thụt lùi và hỗn loạn vì cuộc ``đại nhảy vọt'' và cuộc ``đại cách mạng văn hóa'', đòi hỏi phải được cấp bách ổn định và cải thiện. Mặc khác, sự yếu kém về quân sự và kinh tế của Trung quốc không cho phép những người cầm quyền ở Bắc kinh thực hiện mưu đồ như họ mong muốn. Cho nên, về đối nội, họ lấy tư tưởng đại dân tộc để tập hợp các phe phái và động viên nhân dân Trung quốc nhằm thực hiện kế hoạch ``bốn hiện đại hóa''. Về đối ngoại, họ dấn sâu vào con đường phản động, lợi dụng lúc chủ nghĩa đế quốc bị khủng hoảng về kinh tế và chính trị nghiêm trọng, và lúc Mỹ phải điều chỉnh chiến lược toàn cầu, để câu kết với chủ nghĩa đế quốc, chống Liên Xô, chống phong trào cách mạng thế giới, kiếm nhiều vốn và kỹ thuật của phương Tây cho kế hoạch ``bốn hiện đại hóa''. Khẩu hiệu ``chống bá quyền'' của họ chỉ là chiêu bài để che đậy chiến lược phản cách mạng và chính sách bành trướng đại dân tộc và bá quyền nước lớn của họ.

Họ hằn học nhìn thắng lợi của nhân dân Việt Nam, cho nên, từ khi nhân dân Việt Nam giành được toàn thắng, họ càng ngày càng công khai và điên cuồng thực hành một chính sách thù địch toàn diện và có hệ thống chống nước Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam.

[anh7]
[anh8]

Đây là một văn kiện tuyệt mật của Quân ủy trung ương Đảng Cộng Sản Trung Quốc do Lê Xuân Thành, sinh ngày 20-3-1949 tại Quảng Đông Trung Quốc cung khai.

Thành là công an của Trung Quốc, trú quán tại 165 đường Hồng Ký, khu Tuyên Vũ, thành phố Bắc Kinh. Tháng 3 năm 1973 chạy sang Việt Nam làm gián điệp. Ngày 30-3-1973 bị bắt ở xã Ngư Thủy, huyện Quảng Ninh, tỉnh Quảng Bình. Trong văn kiện đó có một đoạn như sau:

"...Nước ta và nhân dân Việt Nam có mối hận thù dân tộc hàng nghìn năm nay.

... Chúng ta không được coi họ là đồng chí chân chính của mình, đem tất cả vốn liếng của ta trao cho họ. Ngược lại chúng ta phải tìm mọi cách làm cho nước họ ở trong tình trạng không mạnh, không yếu mới có thể buộc họ ở trong tình trạng hiện nay.

... Vẽ bề ngoài chúng ta đối xử với họ như đồng chí của mình, nhưng trên tinh thần ta phải chuẩn bị họ trở thành kẻ thù của chúng ta..."

## II. ĐIÊN CUỒNG CHỐNG VIỆT NAM NHƯNG CÒN CỐ GIẤU MẶT

### 1. Thông qua bè lũ Pôn Pốt – Lêng Xa-ry tiến hành cuộc chiến trnh biên giới chống Việt Nam ở phía tây nam Việt Nam.

Ngay từ giữa những năm 1960, những người lãnh đạo Trung Quốc đã mưu tính nắm trọn vấn đề Campuchia, trước mắt nhằm phá hoại Mặt trận đoàn kết các nước Đông Dương, làm yếu cuộc kháng chiến của nhân dân Việt Nam, có thể đàm phán với Mỹ và lâu dài nắm bắt Campuchia lệ thuộc và trở thành một bàn đạp của Trung Quốc để bành trướng xuống Đông Dương và Đông nam Châu Á. Sau ngày 17 tháng 4 năm 1975, nước Campuchia hoàn toàn thoát khỏi ách thống trị của bè lũ Lon non, tay sai của Mỹ, họ dùng bọn tay sai Pôn Pốt – Lêng-xa-ry chiếm quyền lãnh đạo đảng Cọng sản Campuchia, gạt quốc trưởng Xihanuc và những người thân cận của ông ta, để xây dựng nên một chế độ phát xít diệt chủng có một không hai trong lịch sử loài người và thông qua chế độ đó hoàn toàn kiểm soát nước Campuchia, biến Campuchia thành một nước chư hầu kiểu mới và căn cứ quân sự của họ để tiến đánh Việt Nam từ phía tây nam.

Họ đã đổ tiền, vũ khí và dụng cụ chiến tranh các loại và đưa hàng vạn cố vấn Trung Quốc vào campuchia để thành lập hàng chục sư đoàn mới gồm đủ bộ binh, thiết giáp, pháo binh, xây dựng thêm hoặc mở rộng nhiều căn cứ hải quân, không quân, hệ thống kho hậu cần.

Dưới sự đạo diễn của Bắc Kinh, tập đoàn cầm quyền phản động Phnông Pênh lúc đó tiến hành một chiến dịch tuyên truyền rộng khắp vu khống Việt Nam ``xâm lược Campuchia'', ``âm mưu ép Campuchia vào liên bang Đông Dương do Việt Nam khống chế'', ráo riết hô hào chiến tranh chống Việt Nam. Chúng đã phá hoại cuộc đàm phám giữa hai nước nhằm giải quyết vấn đề biên giới để có cớ duy trì một tình hình ngày càng căng thẳng ở vùng biên giới Việt Nam – Campuchia. Ngay từ tháng 4 năm 1975, chúng đã đưa quân lấn chiếm, bắn phá nhiều điểm trên lãnh thổ Việt Nam và từ đó ngày càng gây thêm nhiều vụ xung đột ở biên giới, đột kích nhiều đồn biên phòng, làng xóm Việt Nam, làm cho tình hình biên giới không ổn định, ngăn cản nhân dân Việt nam khôi phục và xây dựng kinh tế. Từ những cuộc khiêu khích vũ trang, chúng tiến đến gây ra một cuộc chiến tranh biên giới chống nước Việt Nam từ tháng 4 năm 1977 suốt dọc hơn 1.000 km với những cuộc tiến công qui mô, huy động hàng vạn bộ binh có xe tăng, trọng pháo yểm trợ, có khi vào sâu lãnh thổ Việt Nam hơn 30 km, giết hại dã ma dân thường, tàn phá nhà cửa, hoa màu, gây nên biết bao tội ác không thể dung thứ được.

[anh9]

Chỉ thị của Kiều Ủy trung ương Trung Quốc về công tác đối với người Hoa ở Việt Nam năm 1996.

Có đoạn viết :

‘..... Phải lấy tư tưởng Mao Trạch Đông giáo dục Hoa kiều ở Việt Nam.

Báo Tân Việt Hoa phải trở thành trận địa tuyên truyền tư tưởng Mao Trạch Đông.

Hoa kiều nhất định phải có ``chủ nghĩa nước lớn''. Các đồng chí phải chủ động quan hệ chặt chẽ với đại sứ quán để đại sứ quán giúp đỡ được tốt hơn''.

### 2. Dùng vấn đề người Hoa để chống đối Việt Nam từ bên trong

Ở Việt Nam có khoảng 1 triệu 20 vạn người Hoa sinh sống; gần một triệu người ở miền Nam và trên 20 vạn người ở miền Bắc, năm 1955, Đảng Lao động Việt Nam và Đảng Cọng Sản Trung Quốc đã thỏa thuận là người Hoa ở miền Bắc là do Đảng Lao động Việt Nam lãnh đạo và dần dần chuyển thành công dân Việt Nam. Trong 20 năm qua, người Hoa ở miền bắc được hưởng quyền lợi và làm nghĩa vụ như công dân Việt Nam. Còn người Hoa ở miền nam, từ năm 1956 dưới chính quyền Ngô Đình Diệm, đã vào quốc tịch Việt Nam để được hưởng nhiều điều kiện dễ dàng trong việc làm ăn sinh sống.

Sau khi giải phóng hoàn toàn miền nam, Chính phủ và nhân dân Việt Nam tiếp tục nghiêm chỉnh thực hiện sự thỏa thuận năm 1955 giữa hai đảng về người Hoa ở miền bắc, đồng thời tôn trọng thực tế lịch sử về người Việt gốc Hoa ở miền nam, coi người Hoa ở cả hai miền là một bộ phận trong cộng đồng trong dân tộc Việt Nam. Số ít người mang căn cước Đài Loan, Hồng Kông hoặc quốc tịch nước khác và số Hoa kiều bị bọn Pôn Pốt – Lêng xa ry xua đuổi và tỵ nạn ở miền nam Việt nam thì được coi là ngoại kiều.

Ngược lại, những người cầm quyền Trung Quốc xuyên tạc sự thỏa thuận năm 1955 giữa hai đảng, phủ nhận thực tế lịch sử về những người Việt gốc Hoa ờ miền nam Việt Nam, coi tất cả kiều dân ở hai miền là kiều dân Trung Quốc để đòi quyền lãnh đạo những người ấy. Trên thực tế, họ lập các tổ chức phản động và mạng lưới gián điệp người Hoa trên đất Việt Nam. Các tổ chức gọi là ``Hoa kiều hòa bình liên hiệp hội'', ``Hoa kiều tiến bộ'', ``Hoa kiều cứu vong hội'', ``Đoàn thanh niên chủ nghĩa Mac – Lenin'', ``Hội học sinh Hoa kiều yêu nước'', ``Mặt trận thống nhất Hoa kiều''..,v.v.. do Bắc Kinh thành lập và chỉ huy, đã hoạt động chống lại các chính sách của chính quyền, chống lệnh đăng ký nghĩa vụ quân sự, chống việc đi xây dựng vùng kinh tế mới, kích động tâm lý huyết thống trong người Việt gốc Hoa, khơi lên phong trào đòi trở lại quốc tịch Trung Quốc. Họ in tiền giả, đầu cơ tích trữ, nâng giá hàng nhằm phá kế hoạch ổn định và phát triển kinh tế của các cơ quan Nhà nước ở miền Nam Việt Nam, Với những thủ đoạn đó, những người cầm quyền Bắc Kinh đã gây thêm khó khăn cho nhân dân miền Nam Việt Nam vốn đã gặp biết bao khó khăn do 30 năm chiến tranh của đế quốc để lại, khiến cho nhiều người về sau bỏ nước ra đi tìm một nơi mà họ cho là dễ làm ăn hơn. Bắc Kinh đã dùng người Hoa làm công cụ gây rối loạn về chính trị, kinh tế, xã hội ở Việt Nam như họ đã làm ở một số nước Đông Nam châu Á và Nam Á.

### 3. Dùng vấn đề viện trợ để nâng thêm sức ép

Năm 1973. những người lãnh đạo Trung Quốc đã long trọng hứa sẽ viện trợ cho Việt Nam ít nhất trong 5 năm liền ở mức kim ngạch năm 1973.

Năm 1975, khi chào mừng nhân dân Việt Nam giải phóng hoàn toàn miền Nam Việt Nam, những người lãnh đạo Trung Quốc cũng nói ``sẽ tiếp tục làm tròn nghĩa vụ quốc tế của mình, kiên quyết ủng hộ sự nghiệp chính nghĩa nhằm củng cố thành quả thắng lợi, thống nhất và xây dựng Tổ Quốc của nhân dân Việt nam''. Thật ra đây chỉ là lời tuyên bố giả dối để che dấu sự hằn học của họ đối với sự thắng lợi lịch sử của nhân dân Việt Nam đang làm nức lòng tất cả những lực lượng cách mạng và tiến bộ trên toàn thế giới, che giấu những kế hoạch đen tối của họ nhằm chống lại Nước Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam.

Trên thực tế, họ không ủng hộ nhân dân Việt Nam xây dựng lại đất nước trong giai đoạn mới.

Nếu những năm 1960 -1970, những người lãnh đạo Trung Quốc giảm viện trợ đối với Việt Nam vì họ không tán thành Việt Nam thương lượng với Mỹ để kéo Mỹ xuống thang chiến tranh, và nếu những năm 1971 -1972 họ tăng viện trợ đối với Việt Nam cao nhất so với các năm trước, vì họ muốn lợi dụng vấn đề Việt Nam để thương lượng với Mỹ, thì đến năm 1975, vì bị thất bại ở miền Nam Việt Nam, họ lại dùng viện trợ để gây sức ép đối với Việt Nam. Họ khước từ những yêu cầu viện trợ mới của Việt Nam. Về phần viện trợ cũ đã thỏa thuận từ trong chiến tranh và chưa giao hết, họ vin cớ này cớ khác để dây dưa, trong đó có những công trình đang làm dở, có những công trình rất quan trọng đối với công cuộc hòa bình xây dựng đất nước Việt nam. Rõ ràng viện trợ của những người lãnh đạo Trung Quốc không phải là ``vô tư'' như họ thường khoe khoang, mà chỉ là công cụ của chủ nghĩa bành trướng đại dân tộc và bá quyền nước lớn, Viện trợ của Bắc Kinh chỉ là ``cái gậy và củ cà rốt''..

Những thủ đoạn nói trên của Bắc Kinh nhằm chống Việt Nam là những đòn thâm độc, quyết liệt, nhưng đều đã thất bại: bộ mặt phát xít diệt chủng của Pôn Pốt – lêng xa ry bị nhân dân Campuchia và cả của loài người tiến bộ lên án; kế hoạch dùng người Hoa phá Việt Nam từ bên trong bị phá sản; mưu đồ gây sức ép bằng viện trợ và các thủ đoạn khác không lay chuyển được đường lối độc lập, tự chủ của Việt Nam, không khuất phục được nhân dân Việt Nam.



## III. ĐIÊN CUỒNG CHỐNG VIỆT NAM MỘT CÁCH CÔNG KHAI

Giấu mặt chống Việt Nam không đạt kết quả mong muốn, những người cầm quyền Trung Quốc quay ra công khai chống Việt Nam bằng mọi thủ đoạn, kể cả bằng đe dọa vũ lực và dùng vũ lực.

### 1. Cái gọi là vấn đề ``nạn kiều''

Đầu năm 1978, những người cầm quyền Trung Quốc dựng lên cái gọi là vấn đề ``nạn kiều'' để mở đầu một chiến dịch quy mô, công khai chống Nước Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam.

Sự thật là chính các tổ chức bí mật người Hoa, mạng lưới gián điệp của sứ quán Trung Quốc ở Hà Nội được sự chỉ đạo hàng ngày, hàng giờ của bộ máy tuyên truyền Bắc Kinh, bằng những sự bịa đặt trắng trợn, những luận điệu vu cáo Việt Nam ``xua đuổi, bài xích, khủng bố người Hoa'', bằng những thủ đoạn lừa gạt, mua chuộc, dụ dỗ, dọa nạt, đã gây nên trong quần chúng người Hoa đang làm ăn yên ổn ở Việt Nam một tâm trạng hoang mang, lo sợ chiến tranh sắp nổ ra, một tâm lý nghi ngờ, thậm chí thù ghét người Việt Nam, khiến họ ồ ạt kéo đi Trung Quốc. Bọn tay chân của Trung Quốc tổ chức cho những người đó vượt biên giới trái phép rồi lại chặn họ lại, gây ra ùn tắc ở biên giới Việt – Trung để dễ bề kích động họ chống lại và hành hung những nhà chức trách Việt Nam ở địa phương, Lúc dòng người Hoa ùn ùn kéo đi Trung Quốc, Bắc Kinh lại trắng trợn đưa hai tàu sang Việt Nam đón ``nạn kiều'', mặc dầu họ không nêu trước vấn đề đó với Chính phủ Việt Nam. Chỉ trong mấy tháng đầu, 17 vạn người Hoa đã rời Việt Nam đi Trung Quốc. Cái gọi là vấn đề ``nạn kiều'' chỉ là một sự cưỡng bức người Hoa ở Việt Nam ồ ạt di cư đi Trung Quốc mà thủ phạm chính là tập đoàn phản động trong giới cầm quyền Bắc Kinh, một sự lừa gạt và một sự phản bội của họ nhằm gây xáo trộn về chính trị, xã hội, kinh tế ở Việt Nam, hòng khuất phục nhân dân Việt Nam, đồng thời kích động dư luận Trung Quốc, chuẩn bị sẵn sàng ``đạo quân thứ năm'' cho việc tiến hành xâm lược Việt Nam trong bước sau.

[anh10]

Một thư của sứ quán Trung Quốc ở Hà Nội xúi giục người Hoa ở Đà nẵng chống Việt Nam, tịch thu được của can phạm Hàng Phú Quang bị bắt tháng 7 năm 1978.

Dòng chữ đầu của góc bên phải là ``Thư của sứ quán''. Trích một số đoạn:

1. Toàn thể Hoa kiều tại miền Trung, Nam, Bắc Việt Nam phải đoàn kết, nhất trí để đối phó với hoàn cảnh ác liệt.

2.	Phải chú ý theo dõi chỉ thị qua Đài phát thanh của Tổ quốc.

3.	Hoa kiều về nước thì phải đợi chỉ thị, chờ nhân viên ngoại giao đến có chỉ thị rõ ràng để lo liệu. Sau đó tuần tự lên tàu về nước.

4.	Không nghe chính quyền địa phương lừa gạt ghi là người Việt gốc Hoa làm thủ
tục xuất cảnh về Trung Quốc. Vì như vậy thì sẽ bị tổn thất nặng nề về động sản
và bất động sản của tư nhân\*. Phải chờ nhân viên ngoại giao của ta chỉ dẫn cách điền vào đơn (hai bên chính phủ đã bàn) để tài sản công tư khỏi bị tổn thất.

5.	Đề phòng số phần tử xấu tung tin đồn ly gián làm dao động ý chí của Hoa kiều.

----

Vốn là người làm ăn sinh sống lâu đời ở Việt Nam, thông thạo địa hình, phong tục, tập quán, có nhiều cơ sở quen thuộc cũ, có nhiều khả năng nắm được nhiều tin tức, tình hình, những người Hoa ở Việt Nam đi Trung Quốc đã được bọn bành trướng Bắc Kinh chọn lựa để đưa vào những ``sư đoàn sơn cước'' chuyên đánh rừng núi, thọc sâu vào hậu phương, hoặc những đơn vị đi trước mở đường, hoặc những đơn vị thám báo, dẫn đường, bắt cóc, ám sát, phá hoại cầu cống, kho tàng của Việt Nam. Nhiều tên trong bọn chúng đã bị bắt trong cuộc chiến tranh xâm lược Việt Nam từ ngày 17 tháng 2 năm 1979.

Trước quyết tâm của nhân dân Việt Nam giữ vững chủ quyền của mình, những người cầm quyền Bắc Kinh buộc phải rút lui hai chiếc tàu đi đón ``nạn kiều'' về nước, ngồi vào đám phán với phía Việt Nam về việc giải quyết vấn đề người Hoa. Nhưng trong đàm phán, họ vẫn giữ thái độ nước lớn, ngang ngược áp đặt quan điểm vô lý của họ, bất chấp chủ quyền của nước Cọng hòa xả hội chủ nghĩa Việt Nam và luật pháp quốc tế. Chính họ đã cố tình phá hoại cuộc đàm phán đó để tiếp tục dùng vấn đề người Hoa chống Việt Nam.

### 2. Cắt viện trợ, rút chuyên gia

Trong lúc bọn bành trướng Bắc Kinh ráo riết dụ dỗ, cưỡng ép người Hoa ở Việt Nam đi Trung Quốc, họ dùng cái gậy ``viện trợ'' để đồng thời đánh Việt Nam về kinh tế. Chỉ trong hơn một tháng, bất chấp luật pháp và tập quán quốc tế, họ đơn phương tuyên bố chấm dứt toàn bộ viện trợ kinh tế và kỹ thuật cho Việt Nam, gọi về nước tất cả các chuyên gia và cán bộ kỹ thuật Trung Quốc đang công tác ở Việt Nam. Đây là một đòn cực kỳ thâm độc, đưa ra đúng lúc nhân dân Việt Nam đang phải hàn gắn những vết thương chiến tranh, đồng thời vừa phải đối phó với cuộc chiến tranh biên giới ở phía tây nam, khắc phục những khó khăn kinh tế do việc gần 20 vạn người Hoa đột nhiên bỏ ruộng đồng, nhà máy đi Trung Quốc, vừa phải đối phó với những thiệt hại nghiêm trọng do những trận bão, lụt nặng nề nhất ở Việt Nam trong hàng trăm năm qua gây ra.

Đi đôi với việc cắt viện trợ, rút chuyên gia, tạp đoàn phản động trong giới cầm quyền Trung Quốc còn ngang nhiên vận động các nước, các tổ chức quốc tế ngưng viện trợ cho công cuộc xây dựng lại Việt Nam. Lòng dạ họ xấu xa, thâm độc đến thế là cùng!

Họ đẩy mạnh việc vu cáo Việt Nam để vừa che đậy ý đồ bành trướng của họ ở Đông nam châu Á, vừa cản trở quan hệ bình thường giữa Việt Nam và các nước thuộc tổ chức ASEAN, kêu gọi các nước đó lập ``mặt trân chung với Trung Quốc'' chống Việt Nam. Với cuộc vận động đó, họ hy vọng trên thực tế sẽ thực hiện được chính sách bao vây về kinh tế, cô lập về chính trị, tiến công về quân sự, như bọn đế quốc và thực dân vẫn làm bấy lâu nay với một số nước. Đây là một hành động thô bạo không những xâm phạm độc lập, chủ quyền của Việt Nam, mà còn can thiệp vào công việc của các nước khác và các tổ chức quốc tế.

### 3. Duy trì tình hình căng thẳng ờ biên giới Việt Nam

Song song với việc hoạt động phá hoại kinh tế, chính trị, những người cầm quyền Trung Quốc ráo riết tăng cường sức ép quân sự đối với nước Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam từ mọi phía.

Ở phía bắc, họ đưa quân ra vùng biên giới Việt – Trung, tăng cường những vụ khiêu khích vũ trang lấn chiếm lãnh thổ Việt Nam, xâm phạm chủ quyền và toàn vẹn lãnh thổ của Việt Nam. Tạo nên tình hình thường xuyên căng thẳng ở vùng biên giới. Nếu số vụ khiêu khích lấn chiếm lãnh thổ Việt Nam của họ năm 1975 là 234 vụ, gấp rưỡi năm 1974, thì năm 1978 tăng vọt lên 2.175 vụ, gấp mười lần.

Ở phía tây nam, theo mệnh lệnh của Bắc Kinh, bè lũ diệt chủng Pôn Pốt-lêng xa ry khước từ các đề nghị của Việt Nam về việc hai bên thành lập một khu phi quân sự ở vùng biên giới, ca1chly quân độ của mình và ký một hiệp ước hữu nghị không xâm lược lẫn nhau, không can thiệp vào nội bộ của nhau, để kiếm cớ duy trì cuộc chiến tranh biên giới chống Việt Nam, đồng thời chuẩn bị những cuộc phiêu lưu quân sự qui mô lớn sau này.

Ở phía tây, những người cầm quyền Trung Quốc bất chấp luật pháp quốc tế, ngày càng tăng cường gây sức ép đối với nước Cộng hòa dân chủ nhân dân dân Lào, một nước nhỏ hơn Trung Quốc luôn luôn theo đuổi theo chính sách hòa bình, hữu nghị cới các nước láng giềng. Họ nuôi dưỡng bọn tàn quân của lực lượng đặc biệt Mèo do CIA tổ chức và chỉ huy trước đây, thông qua đạo quân làm đường của họ để tìm cách can thiệp sâu vào các tỉnh Bắc Lào, vu cáo Việt Nam ``thôn tính'' Lào, chia rẽ Lào với Việt Nam, đưa nhiều sư đoàn quân áp sát biên giới Lào – Trung. Mục tiêu của họ là để tăng thêm sự uy hiếp Việt Nam về quân sự từ phía tây, đồng thời làm suy yếu và khống chế từng bước Lào.

### 4. Tấn công Việt Nam từ hai hướng

Những mưu đồ trên đây rất thâm độc và có thể gây khó khăn cho nhân dân Việt Nam, nhưng đều đã thất bại, cho nên cuối năm 1978 và đầu năm 1979, những người vầm quyền Trung Quốc phải tính đến việc tấn công quân sự nước chxh từ hai hướng.

Ở phía tây nam, theo kế hoạch của Bắc Kinh, sau khi tập trung 19 sư đoàn bộ binh (trong tổng số 23 sư đoàn bộ binh) đếnsát biên giới Việt Nam, ngày 22 tháng 12 nă, 1978, bè lũ Pôn pốt – lêng xa ry đã xử dụng những sư đoàn tinh nhuệ nhất của chúng, có xe tăng và pháo binh yểm trợ, đánh vào khu vực Bến Sỏi thuộc tỉnh Tây Ninh (cách Sài gòn 100 km, với ý đồ đánh chiếm chớp nhoáng thị xã Tây Ninh, mở đường tiến sâu vào miền Nam Việt Nam, đồng thời làm yếu Việt Nam để quân Trung Quốc đánh vào Việt Nam từ phía bắc.

Thực hiện quyền tự vệ chính đáng của mình, nhân dân Việt Nam đã làm thất bại hoàn toàn kế hoạch quân sự đó. Đồng thời, quân và dân Campuchia dưới sự lãnh đạo của Mặt trận đoàn kết dân tộc cứu nước Campuchia, được sự giúp đỡ và ủng hộ của nhân dân Việt Nam, đã vươn lên đập tan chế độ diệt chủng tàn bạo Pôn pốt – lêng xa ry và cái gọi là ``chính phủ Campuchia dân chủ'', và ngày 10 tháng 1 năm 1979 lập nên chế độ Cọng hòa nhân dân Campuchia, thật sự đại diện cho nhân dân Campuchia.

Ở phía bắc, những người cầm quyền Trung Quốc đã huy động 60 vạn quân, gồm nhiều quân đoàn và sư đoàn độc lập, nhiều đơn vị binh chủng kỹ thuật với gần 800 xe tăng, hàng ngàn khẩu pháo, hàng trăm máy bay các loại của hầu hết các quân khu của Trung Quốc, điên cuồng phát động chiến tranh xâm lược Việt Nam ngày 17 tháng 2 năm 1979 trên toàn tuyến biên giới dài hơn 1000 km. Quân của bọn phản động Trung Quốc đi đến đâu là tàn sát dân thường, kể cả phụ nữ, trẻ sơ sinh, phá hủy triệt để các bản làng, chùa chiền, nhà thờ, trường học, vườn trẻ, bệnh viện, nông trường, lâm trường v.v.. Chúng đã hành động với sự man rợ của một đạo quân ăn cướp thời trung cổ kết hợp với những thủ đoạn tinh vi của các đội quân xâm lược của đế quốc ngày nay.

Để lừa gạt dư luận Trung Quốc và dư luận thế giới, những người cầm quyền Bắc Kinh đã tuyên bố rằng đấy chỉ là một cuộc ``phản kích để tự vệ'' bằng những đơn vị biên phòng. Sự thật đây là một cuộc chiến tranh xâm lược toàn diện bằng lực lượng chính quy của hầu hết các quân khu của Trung Quốc, có chuẩn bị kỹ càng về các mặt, từ việc xây dựng những công trình quân sự, đường sá, hầm hào, sân bay dọc biên giới Việt – Trung đến việc vu cáo Việt Nam, phá hoại tình hữu nghị Việt – Trung, kích động tư tưởng đại dân tộc trong nhân dân Trung Quốc hòng biện bach và che dấu hành động xâm lược của họ. Về mặt đối ngoại, họ cũng chuẩn bị chu đáo, đặc biệt là họ đã phát động cuộc chiến tranh xâm lược sau khi Phó thủ tướng Đặng Tiểu Bình đi thăm Mỹ và Nhật Bản về, thực tế là sau khi tranh thủ được sự đồng tình của Mỹ và Nhật. Mục tiêu đầy tham vọng của họ là: tiêu diệt một bộ phận lực lượng vũ trang của Việt Nam, phá hoại tiềm lực quốc phòng và kinh tế, chiếm đất đai của Việt Nam, kích động bạo loạn.

Hai cuộc chiến tranh xâm lược Việt Nam mà những người cầm quyền Trung Quốc gây ra từ hai hướng là bước leo thang cao nhất trong cả một quá trình hành động tội ác chống độc lập, chủ quyền, thống nhất và toàn vẹn lãnh thổ của nhân dân Việt Nam từ trước đến nay nhằm làm yếu, thôn tính và khuất phục Việt Nam. Trái với mọi tính toán của Bắc Kinh, cuộc chiến tranh xâm lược của họ đã thất bại thảm hại, đã bị toàn thế giới lên án và một bộ phân nhân dân Trung Quốc phản đối. Ngày 5 tháng 3 năm 1979 họ đã buộc phải tuyên bố rút quân, và sau đó đã phải nhận ngồi đàm phán với phía Việt Nam.

#### 5. Tiếp tục chống Việt Nam bằng nhiều thủ đoạn

Những người cầm quyền Trung Quốc đã tuyên bố rút quân về bên kia biên giới, nhưng thực tế cho đến nay quân của họ tiếp tục chiếm đóng hơn mười điểm trên lãnh thổ Việt Nam, xây dựng thêm công sự ở các nơi đó, vi phạm trắng trợn đường biên giới do lịch sử để lại mà cả hai bên đã thỏa thuận tôn trọng.

Suốt dọc biên giới Việt – Trung, họ tiếp tục bố trí nhiều quân đoàn có pháo binh và thiết giáp yểm trợ, tăng cường các phương tiện chiến tranh, ra sức xây dựng các công trình quân sự, thường xuyên diễn tập quân sự, tung các đội thám báo, biệt kích xâm nhập nhiều khu vực của Việt Nam. Không ngày nào họ không gây ra những vụ khiêu khích vũ trang, nổ súng, gài mìn, bắn giết nhân dân địa phương. Có nơi, họ cho bắn súng cối hạng nặng suốt ngày, có nơi họ cho một tiểu đoàn quân chính qui tiến sâu vào lãnh thổ Việt Nam hơn 4 km, bắn giết dân thường, đốt phá nhà cửa và phá hoại hoa màu. Có nơi từng tốp máy bay Trung Quốc bay sâu vào vùng trời Việt Nam từ 8 đến 10 km. Họ bí mật đẩy trở lại Việt Nam những người Hoa đã bị họ cưỡng bức di cư sang Trung Quốc. Những hành động có tính toán đó cùng với những thủ đoạn khác của họ nhằm duy trì tình hình căng thẳng ở vùng biên giới, xâm phạm độc lập, chủ quyền và toàn vẹn lãnh thổ, tiếp tục uy hiếp an ninh của nước Việt Nam. Những người cầm quyền Trung Quốc còn nhiều lần đe dọa ``cho Việt Nam một bài học thứ hai'', thậm chí ``nhiều bài học nữa''. Trên danh nghĩa nào, và dựa vào luật pháp nào mà những người cầm quyền Bắc Kinh có quyền cho Việt Nam và dạy Việt Nam bài học? Việt Nam và Trung Quốc là những nước độc lập, có chủ quyền. Hiến chương Liên hiệp quốc, công pháp quốc tế cũng như tập quán quốc tế tuyệt đối không cho phép Trung Quốc làm bất cứ điều gì phương hại đến độc lập, chủ quyền, toàn vẹn lãnh thổ của Việt Nam cũng như của bất kỳ nước nào khác. Phải chăng vì Trung Quốc nước rộng, người đông mà bọn bành trướng Trung Quốc tự cho phép làm ra luật, đe dọa, khuất phục các mước nhỏ hơn, ít người hơn?

Những người cầm quyền Trung Quốc đã nhận ngồi vào đàm phán với phía Việt Nam để bàn những biện pháp cấp bách bảo đảm hòa bình và an ninh ở vùng biên giới và các vấn đề thuộc quan hệ giữa hai nước. Nhưng ở vòng một tiến hành ở Hà Nội, cũng như vòng hai tiến hành tại Bắc Kinh, phía Trung Quốc vẫn lẫn tránh những đề nghị hợp lý hợp tình của phía Việt Nam, từ chối bản đề nghị của phía Việt Nam về những biện pháp cấp bách nhằm chấm dứt các hoạt động khiêu khích vũ trang và đảm bảo hòa bình, ổn định ở vùng biên giới hai nước, tiền đề cấp thiết cho việc tiếp tục giải quyết các vấn đề khác thuộc quan hệ giữa hai nước. Mặt khác, họ đặt điều kiện tiên quyết là Việt Nam phải từ bỏ đường lối độc lập, tự chủ cũa mình đối với các quần đảo

Hoàng Sa và Trường Sa thì họ mới đi vào bàn bạc các vấn đề khác. Đây là thái độ bá quyền nước lớn: họ đến đàm phán không phải vì bàn bạc một cách bình đẳng và xây dựng nhằm giải quyết các vấn đề tranh chấp, mà buộc phải đối phương phải chấp nhận lập trường của mình. Việc những người cầm quyền Trung Quốc đòi Việt Nam rút quân khỏi Campuchia và Lào, thậm chí nêu ra ``nguyên tắc chống bá quyền'' chẳng qua là để che giấu việc họ đưa quân xâm lược Việt Nam, uy hiếp nước Cộng hòa dân chủ nhân dân Lào và can thiệp vào công việc nội bộ của nước Cộng hòa Campuchia, che giấu bộ mặt bá quyền bỉ ổi của họ nhằm thôn tính ba nước Đông Dương, dùng Đông Dương làm bàn đạp bành trướng xuống Đông nam châu Á.

Gần đây, những người cầm quyền Trung Quốc giương cao chiêu bài ``nhân quyền'' của tổng thống Mỹ Carter, lợi dụng vấn đề người Việt Nam đi ra nước ngoài làm một vũ khí mới để chống Việt Nam. Những người Việt Nam đi ra nước ngoài phần lớn là những nhà buôn giàu có và những sĩ quan trước đây sống nhờ Mỹ và chế độ bù nhìn Saigon, những người Hoa do Bắc Kinh dụ dỗ, cưỡng ép ra đi; một số là những người trước đây sống trong xã hội tiêu thụ kiểu Mỹ và nay không chịu nổi những khó khăn do cuộc chiến tranh xâm lược của chủ nghĩa đế quốc Mỹ và sự phá hoại của bọn bành trướng Bắc Kinh gây ra.

Chính phủ Việt Nam vốn có chính sách nhân đạo, tôn trọng nhân quyền, không những đối xử nhân đạo vớn những người Việt Nam đã hợp tác với kẻ thù trong thời gian chiến tranh, mà còn khoan hồng cả đối với những binh sĩ của các đội quân xâm lược bị bắt trong 30 năm qua. Chính phủ Việt Nam hiểu rõ nguyên nhân và hậu quả của vấn đề những người Việt Nam đi ra nước ngoài, đồng thời rất thông cảm các khó khăn của các nước láng giềng trước dòng người di cư đó. Chính vì thế, từ tháng 1 năm 1979, Chính phủ Việt Nam đã tuyên bố cho phép những người có nguyện vọng ra nước ngoài để sum họp gia đình hoặc làm ăn sinh sống được xuất cảnh Một cách hợp pháp sau khi đã làm đầy đủ các thủ tục cần thiết. Mặt khác, các cơ quan có trách nhiệm của Việt Nam đã cùng cơ quan Cao ủy Liên hiệp quốc về vấn đề người tỵ nạn thỏa thuận một chương trình 7 điểm được công bố Ngày 30 tháng 5 năm 1979, nhằm tạo điều kiện dễ dàng cho những người nói trên ra đi một cách có trật tự và an toàn, đồng thời làm giảm bớt khó khăn cho các nước Đông nam châu Á.

Song Bắc Kinh và Washington đều huy động bộ máy tuyên truyền khổng lồ và mọi phương tiện chính trị, kinh tế, tài chính của họ, lợi dụng khía cạnh nhân đạo của vấn đề, sử dụng mọi thủ đoạn gian dối, xuyên tạc, kích động để bóp mép sự thật về vấn đề những người Việt Nam đi ra nước ngoài, phát động một chiến dịch qui mô và cực kỳ ghê tởm chống Việt Nam.

Nhưng thủ phạm đích thực của dòng người ``di tản'' là ai?

Người ta chưa quên rằng Mỹ đã tiến hành một cuộc chiến tranh xâm lược có tính chất hủy diệt chống Việt Nam. Và khi phải rút đội quân viễn chinh của họ, họ để lại ở miền Nam Việt Nam một đất nước tàn phá, một nền kinh tế tê liệt, với trên 3 triệu người thất nghiệp, trên một triệu người bị tàn phế. 80 vạn trẻ em mồ côi, trên 60 vạn gái điếm, trên 1 triệu thanh niên nghiện ma túy. v.v..

Còn những người cầm quyền Bắc Kinh thì sao? Chính họ đã trắng trợn gây ra cái gọi là ``nạn kiều'', cưỡng ép, dụ dỗ người Hoa bỏ nhà bỏ cửa, ruộng đồng, nhà máy để đi Trung Quốc, dùng những tổ chức của Cục tình báo Hoa Nam để quấy rối về chính trị, đầu cơ, tích trữ, nâng giá hàng, in bạc giả, nhằm phá hoại nền kinh tế của Việt Nam, chồng chất thêm khó khăn cho người dân miền Nam Việt Nam. Trong lúc các cơ quan có trách nhiệm của Việt Nam đang cùng Cơ quan Cao ủy Liên hiệp quốc về vấn đề người tỵ nạn tỏ chức việc ra đi có trật tự và an toàn, những người cầm quyền Bắc Kinh cho tai sai tổ chức việc ra đi bất hợp pháp, để kêu la rằng Việt Nam ``xuất cảng nạn dân'', mặc dầu chính họ cho hàng ngàn người Trung Quốc hàng ngày sang Hong kong để từ đó đi các nước Đông nam châu Á, và hoàn toàn không đếm xỉa đến số phận 26.000 Hoa kiều bị bọn Pôn Pốt – lêng-xa-ry trục xuất khỏi Campuchia. Rất tiếc rằng có những chính phủ, tổ chức vì không hiểu sự thật ở Việt Nam, hoặc vì muốn lấy lòng những người cầm quyền Trung Quốc để buôn bán làm ăn, đã phụ họa với chiến dịch kích động và vu cáo của Bắc Kinh !

Chính những kẻ cướp lại đang la làng, những kẻ chà đạp lên nhân quyền và luật pháp quốc tế lại đang giương cao chiêu bài ``nhân đạo'' để thực hiện mục đích chính trị đen tối của họ. Mục đích của Bắc Kinh là xóa nhòa những tội ác tày trời của họ ở Campuchia và trong cuộc chiến tranh xâm lược Việt Nam, che đậy việc họ kích động người Hoa ở Việt Nam ra đi và ``xuất cảng'' hàng chục vạn người Trung Quốc ra nước ngoài, gây khó khăn cho các nước ASEAN, chia rẽ các nước ASEAN với Việt Nam, đánh lạc hướng về nguy cơ của chủ nghĩa bành trướng Trung Quốc và vai trò của ``đạo quân thứ năm'' người Hoa ở Đông nam châu Á.

Nhưng đối với những người có lương tri, sự thật vẫn là sự thật. Hiện nay, ngày càng có nhiều người thấy được thủ đoạn bẩn thỉu của giới cầm quyền phản động Bắc Kinh, tỏ ra thông cảm với những khó khăn và hoan nghênh lập trường đúng đắn của nước Cọng hòa xã hội chủ nghĩa Việt Nam.

Các thế lực đế quốc và phản động, chủ yếu là Washington và Bắc Kinh, đã thất bại trong âm mưu biến Hội nghị quốc tế ở Geneve tháng 7 vừa qua về vấn đề người Đông dương đi ra nước ngoài thành một diễn đàn để vu cáo chống Việt Nam. Những đề nghị của Đoàn đại biểu Chính phủ Cọng hòa xã hội chủ nghĩa Việt Nam tỏ rõ thái độ xây dựng và hợp tác trong việc giải quyết vấn đề những người di cư, đã được sự đồng tình rộng rãi của đại biểu nhiều nước tôn trọng sự thật và công lý, và đã góp phần quan trọng vào thành công của Hội nghị là đặt được nền tảng của một giãi pháp cho vấn đề như ông Tổng thư ký Liên hợp quốc K.Van hem đã kết luận. Nhưng thực tế còn nhiều khó khăn và phức tạp do những hoạt động phá hoại của các thế lực đế quốc và phản động, trước hết là của Washington và Bắc Kinh. Hiện nay, trong lúc Bắc Kinh lớn tiếng đe dọa chiến tranh, trắng trợn đòi ``chủ quyền'' đối với hai quần đảo Trường sa và Hoàng sa, đế quốc Mỹ đưa hạm đội 7 vào hoạt động ngoài khơi Việt Nam không những để khuyến khích những người ra đi bất hợp pháp, mà còn để phối hợp với Bắc Kinh trong những âm mưu đen tối của họ ở khu vực biển Đông và Đông nam châu Á.

---o0o---

Trong 5 năm qua, bằng những thủ đoạn quân sự, chính trị, kinh tế, ngoại giao trực tiếp và gián tiếp, tinh vi và trắng trợn, giấu mặt và công khai, những người cầm quyền Trung Quốc đã không ngừng phá hoại công cuộc xây dựng của nước Cọng hòa xã hội chủ nghĩa Việt Nam, họ càng thất bại thì càng điên cuồng chống Việt Nam hòng khuất phục nhân dân Việt Nam.

Đây là sự phản bội thứ ba của những người lãnh đạo Trung Quốc đối với nhân dân Việt Nam.

# PHẦN THỨ NĂM

CHÍNH SÁCH BÀNH TRƯỚNG CỦA BẮC KINH MỐI ĐE DỌA ĐỐI VỚI ĐỘC LẬP DÂN TỘC HÒA BÌNH VÀ ỔN ĐỊNH Ở ĐÔNG NAM CHÂU Á
===

## I
Việt Nam và Trung Hoa là hai nước láng giềng gần gũi, nhân dân hai nước luôn luôn gắn bó, giúp đỡ, cổ vũ lẫn nhau trong cuộc đấu tranh chống chủ nghĩa đế quốc, vì lợi ích cách mạng của nhân dân mỗi nước. Nhân dân Việt Nam đã ủng hộ nhân dân Trung Hoa về chính trị và tinh thần, có lúc đã phối hợp chiến đấu cùng với nhân dân Trung Hoa trong sự nghiệp giải phóng dân tộc. Nhân dân Trung Hoa, mặc dầu còn có nhiều khó khăn, nhất là trong những năm đầu của nước Cọng hòa nhân dân Trung Hoa, đã dành sự giúp đỡ to lớn cho nhân dân Việt Nam trong hai cuộc kháng chiến chống bọn đế quốc xâm lược.

Nhân dân Việt Nam rất quý trọng và luôn luôn giữ gìn, vun đắp cho mối tình hữu nghị giữa nhân dân Việt Nam và nhân dân Trung Hoa đời đời bền vững. Nhân dân Việt Nam không hề xâm phạm độc lập, chủ quyền, lãnh thổ của nhân dân Trung Hoa, không hề can thiệp vào công việc nội bộ của Trung Hoa. Đối với những vấn đề bất đồng về quan điểm hoặc những hành động sai trái do những người lãnh đạo Trung Hoa gây ra đối với Việt Nam, phía Việt Nam đã cố gắng và bền bỉ tìm cách giải quyết bằng con đường thảo luận nội bộ giữa hai bên.

Mặc dầu những người lãnh đạo Trung Hoa đang tâm phá hoại tình hữu nghị truyền thống giữa nhân dân hai nước, nhân dân Việt Nam không bao giờ quên sự giúp đỡ to lớn mà nhân dân Trung Hoa đã dành cho nhân dân Việt Nam và rất mong muốn tình hữu nghị anh em giữa nhân dân hai nước sớm được khôi phục. Trong các cuộc đàm phán để giải quyết những vấn đề trong quan hệ giữa hai nước, phía Việt Nam luôn luôn xuất phát từ lòng mong muốn thiết tha đó của nhân dân Việt Nam. Lập trường trước sau như một của nước Cọng hòa xã hội chủ nghĩa Việt Nam là sớm khôi phục quan hệ bình thường giữa nước Cọng hòa xã hội chủ nghĩa Việt Nam và nước Cọng hòa nhân dân Trung Hoa trên những nguyên tắc tôn trọng độc lập, chủ quyền, toàn vẹn lãnh thổ của nhau, không can thiệp vào công việc nội bộ của nhau, bình đẳng, tôn trọng lẫn nhau vì lợi ích của mỗi nước và vì lợi ích của hòa bình, ôn định ở Đông nam châu Á và trên thế giới.

# II

[anh11]

Tóm lại, trong 30 năm qua, những người cầm quyền Trung Hoa đã ba lần phản bội nhân dân Việt Nam:

Tại Hội nghị Geneve năm 1954, họ đã bán rẻ lợi ích dân tộc của nhân dân Việt Nam, không những để bảo đảm cho nước họ một vành đai an ninh ở phía nam, mà còn chuẩn bị địa bàn cho việc thực hiện mưu đồ bành trướng ở Đông Dương và Đông nam châu Á. Họ muốn duy trì tình trạng Việt Nam bị chia cắt lâu dài, hòng làm cho Việt Nam suy yếu và phải phụ thuộc vào Trung Hoa.

Trong cuộc kháng chiến chống Mỹ, cứu nước của nhân dân Việt Nam, khi chế độ Ngô Đình Diệm bị sụp đổ thì họ bật đèn xanh cho Mỹ ném bom miền Bắc Việt Nam, đồng thời đưa quân Mỹ trực tiếp xâm lược miền Nam Việt Nam. Khi Việt Nam muốn ngồi vào thương lượng với Mỹ để phối hợp ba mặt trận quân sự, chính trị, ngoại giao thì họ ngăn cản. Khi nhân dân Việt Nam đang trên đà đi tới thắng lợi hoàn toàn thì họ bắt tay với chính quyền Nixon, dùng xương máu của nhân dân Việt Nam để đưa nước Cọng hòa nhân dân Trung Hoa lên địa vị ``siêu cường thứ ba'' và đổi chác lấy việc giải quyết vấn đề Đài Loan.

Sau khi nhân dân Việt Nam giải phóng hoàn toàn miền Nam khỏi ách thống trị thực dân mới của đế quốc Mỹ và thống nhất nước nhà, họ đã dùng mọi thủ đoạn chính trị, quân sự, kinh tế, ngoại giao để làm suy yếu nước Cọng hòa xã hội chủ nghĩa Việt Nam, hòng khuất phục nhân dân Việt Nam, tiến đến dùng lực lượng quân sự của bè lũ tay sai Pôn Pốt – Lêng xa ry xâm lược Việt Nam ở phía tây nam và lực lượng quân sự của Trung Hoa trực tiếp xâm lược Việt Nam ở phía bắc, giết hại nhân dân Việt Nam, phá hoại nghiêm trọng các cơ sở kinh tế, văn hóa của Việt Nam ở các vùng có chiến sự.

Ba lần họ phản bội Việt Nam, lần sau độc ác, bẩn thỉu hơn lần trước!

Đối với nhân dân Lào và nhân dân Campuchia, những người cầm quyền Trung Hoa cũng đã phản bội độc ác và bẩn thỉu. Họ đã hy sinh lợi ích dân tộc của nhân dân Lào và nhân dân Campuchia tại Hội nghị Geneve năm 1954, Trong thời kỳ sau Geneve, họ ngăn cản nhân dân Lào và nhân dân Campuchia đấu tranh cho độc lập dân tộc, hòa bình, trung lập. Khi nhân dân Campuchia hoàn toàn giải phóng đất nước ngày 17 tháng 4 năm 1975, họ đã dùng bọn tay sai Pôn Pốt – Lêng xa ry để thực hiện chính sách diệt chủng, biến nước Campuchia thày một nước chư hầu kiểu mới, một căn cứ quân sự để từ đó tiến công nước Cọng hòa xã hội chủ nghĩa Việt Nam ở phía tây nam. Đối với nước Cộng hòa dân chủ nhân dân Lào, họ phá hoại công cuộc xây dựng hòa bình của nhân dân Lào, trang bị và giúp đỡ các lực lượng phản động ở Lào gây rối loạn, đưa nhiều sư đoàn áp sát biên giới Lào – Trung, hòng ép nhân dân Lào đi vào quỹ đạo của Bắc Kinh. Họ chia rẽ ba nước Việt Nam, Lào, Campuchia, hòng làm suy yếu và thôn timh1 từng nước.

Để che giấu bộ mặt phản bội của họ, những người cầm quyền Bắc Kinh thường hay nhắc đến việc Trung Hoa viện trợ cho nước Việt Nam, thậm chí khoe rằng quân đội của họ đã ``chiến đấu ở Điện Biên Phủ'', v.v.. Nhân dân Trung Hoa đã dành một phần thành quả lao động của mình để giúp đỡ nhân dân Việt Nam kháng chiến chống Pháp, chống Mỹ, xây dựng đất nước, đó là điều mà nhân dân Việt Nam, trong bất cứ tình huống nào, mãi mãi không bao giờ quên. Đối với nhân dân Việt Nam, đó là biểu hiện cao đẹp của mối tình đoàn kết chiến đấu của những người cùng chung cảnh ngộ, nhưng đối với tập đoàn phản động trong giới cầm quyền Bắc Kinh đó là một công cụ chính trị để thực hiện chính sách bành trướng của họ ở Việt Nam cũng như trên toàn bán đảo Đông Dương. Thực tế đã chỉ rõ họ đã xử dụng viện trợ đó khi thì như ``một củ cà rốt'', khi thì như ``một cái gậy'', tùy theo yêu cầu chính trị từng lúc của họ.

Vả lại, không chỉ có vấn đề Trung Hoa giúp đỡ Việt Nam.

Những người lãnh đạo Trung Hoa đã nhiều lần nói rằng nói đến cám ơn thì nhân dân Trung Hoa phải cám ơn nhân dân Việt Nam. Nhân dân Việt Nam đã hi sinh nhiều, cống hiến nhiều đối với nhân dân Trung Hoa. Nhân dân Trung Hoa phải cám ơn và có nghĩa vụ giúp đỡ, ủng hộ nhân dân Việt Nam. Nhân dân hai nước giúp đỡ lẫn nhau.

Về việc Tổng thống Nixon đi thăm Trung Hoa năm 1972, Chủ tịch Mao Trạch Đông đã nói với những người lãnh đạo Việt Nam tháng 6 năm 1973 như sau:

``Thành thực mà nói, nhân dân Trung Hoa, Đảng cọng sản Trung Hoa và nhân dân thế giới phải cám ơn nhân dân Việt Nam đã đánh thắng Mỹ. Các đồng chí chiến thắng mới buộc Nixon phải đi Bắc Kinh''.

Về việc nước Cọng hòa nhân dân Trung Hoa vào Liên Hợp Quốc năm 1971, Thủ tướng Chu Ân Lai đã nói trong cuộc hội đàm với các nhà lãnh đạo Việt Nam tháng 11 năm 1971:

``Cống hiến của Việt Nam rất lớn. Chúng ta gắn bó với nhau''.

Lịch sử - và trước hết là quân đội viễn chinh Pháp – đã trả lời rõ ràng câu hỏi: ai đã chiến đấu và chiến thắng ở Điện Biên Phủ năm 1954? Điều cần nói thêm là trong thời kỳ kháng chiến chống thực dân Pháp xâm lược, Chính phủ Trung Quốc có phái một số cố vấn sang Việt nam, và trong những năm 1960 Bắc kinh có đưa sang Việt Nam một lực lượng gọi là ``bộ đội hậu cần'' để giúp Việt Nam sửa những đoạn đường sắt và đường bộ giáp Trung Quốc bị bom Mỹ phá hỏng và làm một số đường mới ở vùng biên giới hai nước. Nhưng mặt chủ yếu họ làm là điều tra tình hình các mặt, thâm nhập những vùng có các dân tộc thiểu số và tuyên truyền ``cách mạng văn hóa''. Phần lớn những gián điệp và ``bộ đội sơn cước'' mà phía Việt Nam đã bắt được trong tháng 2, tháng 3 vừa qua chính là những tên ``bộ đội làm đường'' Trung Quốc trước đây.

Từ sự phản bội ở Hội nghị Giơ ne vơ năm 1954, việc lợi dụng cuộc kháng chiến chống Mỹ, cưu nước của nhân dân Việt Nam đến việc dựng lên chế độ diệt chủng Pôn Pốt – Lêng xa ry, vũ trang xâm lược Việt Nam và uy hiếp xâm lược Lào, tất cả đều do:

- một tư tưởng chỉ đạo

chủ nghĩa đại dân tộc

- một chính sách

ích kỷ dân tộc

- một mục tiêu chiến lược

chủ nghĩa bành trướng đại dân tộc và chủ nghĩa bá quyền nước lớn

Cụ thể là họ mưu toan thôn tính Việt Nam và toàn bộ Đông Dương, lấy đó làm bàn đạp tiến xuống Đông Nam châu Á và từng bước triển khai chiến lược toàn cầu của họ.

Để đạt mục tiêu và bành trướng bá quyền của họ-những người cầm quyền Bắc Kinh đã nâng lừa dối và bịp bợm thành một quốc sách, một thủ đoạn chiến lược. Về phương diện này, từ chỗ là những người học trò của Gơ ben 1, họ đã trở thành người thầy của Gơ ben. Họ gán cho người khác những điều mà họ muốn làm. Họ đổ vấy cho người khác những điều mà chính họ làm. Họ dựng đứng sự việc, xuyên tạc tài liệu, bóp méo lịch sử. Họ đổi trắng thay đen, đảo ngược phải trái và cứ thế mà tuyên truyền bằng bộ máy thông tin khổng lồ và mọi phương tiện khác. Họ giương cao ngọn cờ của chủ nghĩa xã hội nhưng lại chống chủ nghĩa xã hội. Họ hò hét chống chủ nghĩa đế quốc nhưng chính họ bắt tay với đế quốc Mỹ. Họ kêu la phải chống hai siêu cường nhưng lại cấu kết với đế quốc Mỹ để chống Liên Xô. Họ nói chống chủ nghĩa bá quyền, nhưng chính họ mưu toan thực hiện chủ nghĩa bá quyền ở Đông Dương và Đông nam châu Á. Họ đưa quân xâm lược nước Cọng Hòa Xã Hội Chủ Nghĩa Việt Nam nhưng lại vu cáo Việt Nam ``xâm lược'' Trung Quốc. Họ tỏ vẻ ``bảo vệ nhân quyền'', quan tâm đến ``những người Đông Dương di tản'', nhưng chính họ đã giất hại hàng triệu người Trung Quốc trong cuộc ``đại cách mạng văn hóa'', đã xúi dục hơn 20 vạn người Hoa bỏ Việt Nam đi Trung Quốc. Đối với những người cầm quyền Bắc Kinh, luận điệu của họ là chân lý, lợi ích của họ là đạo lý.''Quan châu được đốt đèn, dân đen không được nổi lửa'', câu nói đó của những người nông dân Trung Quốc dùng trước đây để chỉ trích sự áp bức của bọn bạo chúa phong kiến đã trở thành phương châm của những người cầm quyền Bắc Kinh hiện nay nhằm thực hiện tham vọng bành trướng và bá quyền của họ.

    (Gơ-ben là Bộ trưởng tuyên truyền của Hit le. (BT))

Hiện nay những người lãnh đạo Trung Quốc đang ra sức nêu cao ngọn cờ đại dân tộc để tập hợp các phe phái, thực hiện kế hoạch ``bốn hiện đại hóa''. Về đối ngoại, họ ra sức thực hiện chính sách bành trướng ở Đông Dương và Đông nam châu Á, cấu kết với chủ nghĩa đế quốc và các thế lực phản động khác, trước hết là với Mỹ, chống Liên Xô và cách mạng thế giới, với hy vọng tranh thủ được nhiều vốn và kỹ thuật hiện đại của phương tây, phục vụ cho ``bốn hiện đại hóa''. Và mưu đồ bành trướng và bá quyền của họ.

Một Trung Quốc bị đầu độc bởi tư tưởng đại dân tộc, chủ nghĩa bành trướng và bá quyền của những người cầm quyền, bất kể phát triển theo con đường nào, không phải chỉ đe dọa độc lập dân tộc, chủ quyền và toàn vẹn lãnh thổ của các nước Đông Dương, Đông nam châu Á và Nam Á, đe dọa hòa bình, ổn định ở khu vực này, mà còn đe dọa lợi ích nhiều mặt của các nước khác, kể cả các nước, vì lợi ích trước mắt, đang phụ họa với người cầm quyền Trung Quốc chống Việt Nam, Lào và Cam pu chia. Một số người thức thời trong các nhà chính trị và kinh doanh phương tây mới cảnh cáo chính phủ họ về hậu quả nặng nề có thể xảy ra khi đất nước Trung Quốc lại lâm vào một cuộc khủng hoảng chính trị nội bộ mới, nhưng họ chưa nói tới những hậu quả còn nặng nề gấp bội đối với lợi ích của các nước trên thế giới, do chính sách bành trướng của những người lãnh đạo Trung Quốc gây ra.

Trong hàng nghìn năm qua, nước Việt Nam đã bị các hoàng đế Trung Quốc xâm lược hàng chục lần, nhân dân Việt Nam hiểu rõ những ý đồ đen tối của những người lãnh đạo Trung Quốc, cho nên không một phút nào là không cảnh giác đối với họ. Thậm chí trong lúc đế quốc Mỹ đẩy cuộc chiến tranh xâm lược lên đến đỉnh cao nhất, nhân dân Việt Nam đứng trước những khó khăn chồng chất, nhưng đã thẳng thắn khước từ những đề nghị của những người lãnh đạo Trung Quốc đưa sang Việt Nam 20 vạn quân và số ô-tô cần thiết để đảm nhiệm việc vận chuyển quân sự từ miền Bắc vào miền Nam Việt Nam. Nhân dân Việt Nam luôn luôn giữ vững đường lới độc lập, tự chủ của mình không gì lay chuyển được, bất chấp sức ép dù là che giấu hay công khai, gián tiếp hay là trực tiếp, của những người cầm quyền Trung Quốc.

## III
Mặc dầu cuộc chiến tranh xâm lược Việt Nam tháng 2 năm 1979 đã thất bại thảm hại cả về quân sự và chính trị, những người lãnh đạo Trung Quốc vẫn theo đuổi chính sách điên cuồng chống Việt Nam bằng mọi thủ đoạn. Phía Trung Quốc vẫn giữ thái độ nước lớn trong cuộc đàm phán về những vấn đề thuộc quan hệ giữa hai nước, vẫn ngang ngược đe dọa ``cho Việt Nam một bài học nữa''. Đồng thời họ tìm cách khôi phục chế độ diệt chủng Pôn-Pốt – Lêng Xa ry đã bị nhân dân Cam pu chia lật đổ hoàn toàn, đe dọa xâm lược nước Cộng hòa dân chủ nhân dân Lào, nhằm duy trì sự uy hiếp từ mọi phía đối với Việt Nam.

``Không có gì quí hơn độc lập, tự do''. Nhân dân Việt Nam kiên quyết bảo vệ độc lập, chủ quyền, toàn vẹn lãnh thổ, giữ vững đường lối độc lập, tự chủ và đoàn kết quốc tế đúng đắn của mình, quyết đập tan mọi hành động xâm lược của bất kỳ thế lực phản động nào, quyết làm thất bại mọi mưu đồ bành trướng nhằm khuất phục nhân dân Việt Nam.

Nhân dân Việt Nam có chính nghĩa, lại có sức mạnh vô địch của khối đoàn kết toàn dân, kết hợp với sức mạnh của ba dòng thác cách mạng vĩ đại đã và đang đẩy lùi từng bước các âm mưu can thiệp, nô dịch và xâm lược của chủ nghĩa đế quốc, chủ nghĩa thực dấn cũ và mới, chủ nghĩa bành trướng, và chủ nghĩa bá quyền, ngày càng làm thay đổi bản đồ châu Á, châu Phi, và châu Mỹ La tinh. Bọn bành trướng Bắc Kinh, nếu không sớm rút ra những kết luận cần thiết từ sự thất bại từ chính sách chống Việt Nam vừa qua, thì nhất định sẽ chuốt lấy những thất bại mới nặng nề hơn. Trong thời đại ngày nay, các nước lớn nhỏ đều là bộ phận của một tổng thể duy nhất của xã hội loài người. Bọn bành trướng Bắc Kinh không thể đụng đến Việt Nam mà không khiêu khích cả loài người, không thách thức cả hệ thống xã hội chủ nghĩa, phong trào giải phóng dân tộc và mặt trận nhân dân thế giới vì hòa bình, độc lập dân tộc, dân chủ và tiến bộ xã hội. Nhân dân các nước xã hội chủ nghĩa, các nước độc lập dân tộc và nhân dân yêu chuộng hòa bình và chính nghĩa trên thế giới đã và sẽ đứng về phía nhân dân Việt Nam.

Bằng chính sách bịp bợm ``thân xa đánh gần'' của các hoàng đế Trung Quốc và nhiều thủ đoạn xảo quyệt khác, những người cầm quyền Trung Quốc có thể còn che giấu được bộ mặt bành trướng của họ trong một thời gian. Nhưng sớm muộn nhân dân các nước ở Đông nam châu Á sẽ hiểu rằng chính sách thù địch của Bắc Kinh chống Việt Nam đe dọa độc lập, chủ quyền và toàn vẹn lãnh thổ không phải chỉ của riêng Việt Nam, mà của cả các nước trong khu vực. Mọi người chắc chưa quên rằng Bắc Kinh đã dùng ``đạo quân thứ năm'' người Hoa để gây rối loạn về chính trị, kinh tế ở nhiều nước thuộc Đông nam châu Á trước khi áp dụng ở Việt Nam. Hiện nay trong lúc họ tập trung cố gắng để chống Việt Nam, họ chẳng đang can thiệp một cách thô bạo vào công việc nội bộ của nhiều nước khác ở châu Á đó sao ?

Những người cọng sản chân chính ở Trung Quốc, nhân dân Trung Quốc trong ba mươi năm tồn tại của nước Cộng hòa nhân dân Trung Hoa, đã luôn luôn bị các tập đoàn thống trị lừa ối, sớm muộn sẽ nhận ra chân lý và sẽ đứng về phía nhân dân Việt Nam, sẽ ủng hộ cuộc đấu tranh chính nghĩa của nhân dân Việt Nam.

Cuộc đấu tranh chính nghĩa của nhân dân Việt Nam chống chủ nghĩa bành trướng đại dân tộc và bá quyền nước lớn của tập đoàn phản động trong giới cầm quyền Bắc Kinh để bảo vệ độc lập, chủ quyền, toàn vẹn lãnh thổ của mình, góp phần bảo vệ hòa bình và ổn định ở Đông nam châu Á và trên thế giới tuy lâu dài, gian khổ, nhưng nhất định sẽ thắng lợi vẻ vang.

Nước Việt Nam ngày nay sẽ đứng vững và tiếp tục phát triển trước mọi mưu ma chước quỷ của những người cầm quyền Bắc Kinh, và cũng như nước Việt Nam bốn nghìn năm qua đã đứng vững và phát triển trước những cuộc xâm lược liên tiếp của các hoàng đế Trung Quốc.

Nhân dân Việt Nam và nhân dân Trung Quốc nhất định sẽ sống trong hòa bình, hữu nghị và hợp tác, hợp với nguyện vọng của nhân dân hai nước và hợp với lợi ích của hòa bình ở Đông nam châu Á và trên thế giới.

Tháng 10 năm 1979
(HẾT)

Phụ trách bản thảo đưa in

PHẠM XUYÊN

ĐÌNH LAI

Trình bày và bìa

NGHIÊM THÀNH
