# SỰ THẬT VỀ QUAN HỆ VIỆT NAM - TRUNG QUỐC TRONG 30 NĂM QUA #

Cuốn sách SỰ THẬT VỀ QUAN HỆ VIỆT NAM - TRUNG QUỐC TRONG 30 NĂM QUA là một
văn kiện quan trọng của Bộ Ngoại Giao Nước Cộng Hoà Xã Hội Chủ Nghĩa Việt Nam
được công bố ngày 4 tháng 10 năm 1979 nhằm vạch trần bộ mặt phản động của bọn
bành trướng Bắc Kinh đối với nước ta trong suốt một thời gian dài. Cuốn sách này
gồm toàn văn bản văn kiện nói trên.

# Hoạt động của kho này #

* Mục đích sao chép lại quyển sách trên nhằm dễ dàng trao đổi, lưu trữ, và in
ấn, phổ biến.

* Không thay đổi nội dung của sách, giữ gìn gần nhất những bố cục, trình bày vốn
có của sách.

* Mọi quyền hạn là của tác giả và/hoặc Nhà Xuất Bản.

* Các đóng góp về chỉnh lý, sửa lỗi, vv.. luôn được đón nhận.

* Tải về tại mục
[Downloads](https://bitbucket.org/xuansamdinh/su-that-vietnam-trungquoc/downloads).

* File pdf - A5, fontsize 11pt, xuất bản Desktop.